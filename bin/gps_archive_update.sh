#!/bin/bash

# call with offset days as argument, default -1 day

gps_processing_dir="/ifi-NAS/nes/research/gps"
gps_bin_dir=${gps_processing_dir}"/rtklib_processing/bin"
external_dataproducts_dir=${gps_processing_dir}"/external_dataproducts"
gps_archive_dir=${gps_processing_dir}"/data_archive"
gps_rtk_dir=${gps_processing_dir}"/rtklib_processing"


if [ -z "$1" ]; then
  DoY=$(date +%j -d "-1 days")
  year=$(date "+%y" -d "-1 days")
  Year=$(date "+%Y" -d "-1 days")
#  echo test
else
  j=$(printf "%02d" "$1")
#  echo input $j
  DoY=$(date +%j -d "+$1 days")
  year=$(date "+%y" -d "+$1 days")
  Year=$(date "+%Y" -d "+$1 days")
fi

j=$(date +%j)
#echo "today" $j
#echo $DoY $year

echo GPS Archive Update for Year $Year, single DoY $DoY

# copy swisstopo/pnac files to archive directories
for station_name in HOH2 JUJ2 OALP SAM2 SANB ZERM
do
  station_name_lowercase=${station_name,,}
  echo "data_archive/gps_data/${station_name,,}"
  cd $gps_archive_dir/${station_name}/pnac_rinex_${station_name}
  #RINEX2
  # cp ../swisstopo_auto_delivery/${station_name_lowercase}${DoY}0.${year}d.Z .
  # gunzip -f ${station_name_lowercase}${DoY}0.${year}d.Z
  #RINEX3
  cp ${gps_archive_dir}/swisstopo_auto_delivery/${station_name}00CHE_R_${Year}${DoY}0000_01D_30S_MO.crx.gz .
  gunzip -f ${station_name}00CHE_R_${Year}${DoY}0000_01D_30S_MO.crx.gz
  $gps_bin_dir/crx2rnx -f ${station_name}00CHE_R_${Year}${DoY}0000_01D_30S_MO.crx
  cp -f ${station_name}00CHE_R_${Year}${DoY}0000_01D_30S_MO.crx ${station_name_lowercase}${DoY}0.${year}d
  # ls *.${year}*

  echo "gps/rtklib_processing/pnac/${station_name,,}"
  cd $gps_rtk_dir/pnac/${station_name}
  cp -f $gps_archive_dir/${station_name}/pnac_rinex_${station_name}/${station_name_lowercase}${DoY}0.${year}d .
  $gps_bin_dir/crx2rnx -f ${station_name_lowercase}${DoY}0.${year}d
  rm ${station_name_lowercase}${DoY}0.${year}d
  # convert to uppercase letters
  mv ${station_name_lowercase}${DoY}0.${year}o ${station_name_lowercase^^}${DoY}0.${year}O
  ls *.${year}*
done


# copy ggl files to archive directories
for station_name in HOGR KREB RAND
do
  echo "data_archive/gps_data/${station_name}"
  cd $gps_archive_dir/${station_name}/ggl_rinex_${station_name}
  cp -f ${gps_archive_dir}/ggl_auto_download/${station_name}/${station_name}${DoY}0.${year}D.Z .
  gunzip -f ${station_name}${DoY}0.${year}D.Z
  # ls *.${year}*

  echo "gps/rtklib_processing/cogear/${station_name}"
  cd ${gps_processing_dir}"/rtklib_processing/cogear/"${station_name}
  cp -f ${gps_archive_dir}/ggl_auto_download/${station_name}/${station_name}*.Z .
  gunzip -f ${station_name}*.Z
  # ls ${station_name}${DDoY}0.${year}*
  find ${station_name}*.*D -maxdepth 1 -type f -exec ${gps_bin_dir}/crx2rnx -f {} \;
  rm ${station_name}*.*D
  ls *.${year}*
done

# manual ggl_auto sync
# cd ${gps_archive_dir}
# for n in HOGR KREB RAND; do rsync -travz jbeutel@tik43x.ethz.ch:/usr/whymper/data-tik-02/permasense_vault/data_archive/gps_data/${n}/ggl_rinex_${n} /home/jan.beutel/gps/data_archive/${n}; done
# for n in HOH2 JUJ2 OALP SAM2 SAME SANB ZERM; do rsync -travz jbeutel@tik43x.ethz.ch:/usr/whymper/data-tik-02/permasense_vault/data_archive/gps_data/${n}/pnac_rinex_${n} /home/jan.beutel/gps/data_archive/${n}; done

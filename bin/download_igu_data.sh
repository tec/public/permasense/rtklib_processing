#!/bin/bash
#
# IGS Rapid download script
#
#/bin/echo Input: $1 $2    YEAR DoY


#echo $number_of_opts
i=$((number_of_opts+1))
year=$(eval echo \${$i})
# echo $year
i=$((i+1))
DoYarg=$(eval echo \${$i})
# echo $DoYarg

year=$(date -d "$year-01-01 +$DoYarg days -1 day" "+%Y")
month=$(date -d "$year-01-01 +$DoYarg days -1 day" "+%m")
day=$(date -d "$year-01-01 +$DoYarg days -1 day" "+%d")

# year="2019"
# month="01"
# day="01"

year_short=${year: -2}
sec_in_week="$((60*60*24*7))"
# unix_to_gps_offset=315964800
# unix_to_gps_offset=315968400 
unix_to_gps_offset=315957600

doy=$(date -d ${year}-${month}-${day} +%j) 
dow=$(date -d ${year}-${month}-${day} +%w) 

gps_secs=$(date -d ${year}-${month}-${day} +%s) 

# echo "$unix_to_gps_offset"
if [ $year -ge 2017 ]; then
    leap_seconds="18"
    unix_to_gps_offset=$((unix_to_gps_offset-$leap_seconds))
    # echo "$unix_to_gps_offset"
elif [ $year -ge 2015 ]; then
    if [ month -ge 07 ]; then
        leap_seconds="17"
        unix_to_gps_offset=$((unix_to_gps_offset-$leap_seconds))
    fi
    # echo "$unix_to_gps_offset"
elif [ $year -ge 2012 ]; then
    if [ $month -ge 07 ]; then
        leap_seconds="16"
        unix_to_gps_offset=$((unix_to_gps_offset-$leap_seconds))
    fi
    # echo "$unix_to_gps_offset"
elif [ $year -ge 2009 ]; then
    leap_seconds="15"
    unix_to_gps_offset=$((unix_to_gps_offset-$leap_seconds))
    # echo "$unix_to_gps_offset"
elif [ $year -ge 2006 ]; then
    leap_seconds="14"
    unix_to_gps_offset=$((unix_to_gps_offset-$leap_seconds))
    # echo "$unix_to_gps_offset"
elif [ $year -ge 1999 ]; then
    leap_seconds="13"
    unix_to_gps_offset=$((unix_to_gps_offset-$leap_seconds))
    # echo "$unix_to_gps_offset"
fi

gps_corr_secs="$(($gps_secs-$unix_to_gps_offset))"
gps_week=$((gps_corr_secs / sec_in_week))

echo "Downloading IGS Hourly data $year $year_short $month $day $doy $gps_secs $unix_to_gps_offset $gps_corr_secs $sec_in_week $leap_seconds $gps_week $dow"

# https://cddis.nasa.gov/archive/gps/data/hourly/2020/327/06/
# zim2327g.20n.Z
i=0
for hour in {a..x}
    do
    myhour=$(printf "%02d" $i)
    # echo "${myhour}"
    curl -f -s -R -c ~/.netrc_cookies -n -L -O "https://cddis.nasa.gov/archive/gps/data/hourly/${year}/${doy}/${myhour}/zim2${doy}${hour}.${year_short}n.Z"
    curl -f -s -R -c ~/.netrc_cookies -n -L -O "https://cddis.nasa.gov/archive/gps/data/hourly/${year}/${doy}/${myhour}/zim2${doy}${hour}.${year_short}n.gz"
    # ls -al zim2${doy}*.*
    # gunzip -f -q zim2${doy}${hour}.${year_short}n.Z 
    # gunzip -f -q zim2${doy}${hour}.${year_short}n.gz
    # rm -f zim2${doy}${hour}.${year_short}n.Z
    # rm -f zim2${doy}${hour}.${year_short}n.gz
    i=$((i+1))
done

# ls -al zim2${doy}*.*
gunzip -f -q zim2${doy}*.Z 
rm -f zim2${doy}*.Z
gunzip -f -q zim2${doy}*.gz
rm -f zim2${doy}*.gz

# curl -s -R -c ~/.netrc_cookies -n -L -O "https://cddis.nasa.gov/archive/gps/products/ionex/${year}/${doy}/igrg${doy}0.${year_short}i.Z"
# gunzip -f igrg${doy}0.${year_short}i.Z


for tod in {00,06,12,18}
    do
    # echo "ftp://igs.ign.fr/pub/igs/products/${gps_week}/igu${gps_week}${dow}_${tod}.sp3.Z"
    # curl -f -s -R -n -L -O "ftp://igs.ign.fr/pub/igs/products/${gps_week}/igu${gps_week}${dow}_${tod}.sp3.Z"
    # echo "https://cddis.nasa.gov/archive/gps/products/${gps_week}/igu${gps_week}${dow}_${tod}.sp3.Z"
    curl -f -s -R -c ~/.netrc_cookies -n -L -O "https://cddis.nasa.gov/archive/gps/products/${gps_week}/igu${gps_week}${dow}_${tod}.sp3.Z"
    # ls -al igu*.*
    # gunzip -f -q igu${gps_week}${dow}_${tod}.sp3.Z
    
    # curl -f -s -R -n -L -O "ftp://igs.ign.fr/pub/igs/products/${gps_week}/igu${gps_week}${dow}_${tod}.erp.Z"
    curl -f -s -R -c ~/.netrc_cookies -n -L -O "https://cddis.nasa.gov/archive/gps/products/${gps_week}/igu${gps_week}${dow}_${tod}.erp.Z"
    # ls -al igu*.*
    # gunzip -f -q igu${gps_week}${dow}_${tod}.erp.Z
    
    #experimental ultrarapid
    curl -f -s -R -n -L -O "ftp://igs.ign.fr/pub/igs/products/${gps_week}/igv${gps_week}${dow}_${tod}.sp3.Z"
    # curl -f -s -R -c ~/.netrc_cookies -n -L -O "https://cddis.nasa.gov/archive/gps/products/${gps_week}/igv${gps_week}${dow}_${tod}.sp3.Z"
    # gunzip -f -q igv${gps_week}${dow}_${tod}.sp3.Z
done

gunzip -f -q ig*${gps_week}*.Z

# ls -al zim2${doy}*.${year_short}*
# ls -al ig*${gps_week}${dow}*.*
# chmod 664 *.*

echo "Done. Ready for ice cream."
echo 


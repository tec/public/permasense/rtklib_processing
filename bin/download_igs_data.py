'''
Script to download a file (i.e. from a igs data server).

Usage: python download_igs_data.py [arg1] [arg2]

arg1: The server-path from which to download the file
arg2: The directory in which the file to be downloaded lies
arg3: The name of the file to be downloaded
arg4: The directory into which the data is downloaded
'''

from urllib.request import urlopen
import datetime,sys,os

#download a file
#input: path: server from which to download
#		folder: directory in which the data is stored
#		filename: the name of the file to be downloaded
#		target: the directory into which the data is downloaded
#return: Message indicating if everything worked or errors occurred
def download(path, folder, filename, target):
	try:
		completepath=path+folder+filename
		log_message="Download data from: "+completepath+'\n'
		f = open(target+"/"+filename, 'wb')
		response = urlopen(completepath)
		data=response.read()
		f.write(data)
		f.close()
		#log_message=log_message+"The downloaded data have been written into: "+target+"/"+filename+'\n'
		return log_message
	except:
		log_message="Failed to download from "+completepath
		print(log_message)
		sys.exit(1)

#This script's main function
def main():
	if len(sys.argv)==5:
		path=sys.argv[1]
		folder=sys.argv[2]
		file=sys.argv[3]
		target=sys.argv[4]
		log_message=download(path, folder ,file, target)
	else:
		log_message="Incorrect arguments in download_igs.data.py call! Please refer to the script for the correct usage"
		print(log_message)
		sys.exit(1)

	file_size = str(os.path.getsize(target+"/"+file))
	log_message=log_message+"The downloaded data have been written into: "+str(target)+"/"+str(file)+" File size: "+file_size+'\n'+'\n'
	print(log_message)

if __name__ == "__main__":
   main()

'''
Class holding a string. To the string one can append more strings
'''

import urllib3,datetime,sys,time,getopt,math
from io import StringIO

# class holding a string to which more strings can be appended
class Stringbuilder():
	string = None
	def __init__(self,firstword):
		self.string = str(firstword)

	def append(self,nextword):
		self.string = self.string+str(nextword)
	
	def str(self):
		return self.string

	def getStringbuilder():
		sb = Stringbuilder('')
		return sb

# main function, never used
def main():
	sb =Stringbuilder()
	

if __name__ == "__main__":
   main()

'''
Script to read a configuration file.

Usage: python ConfigReader.py [arg1] [arg2] [arg3]
arg1: configfile, the file in which the key-value pair is stored
arg2: section; the section in which the key-value paor is storde
arg3: key: the key whose value is required
'''
import os.path
import sys
# sys.path.append('.')
import configparser as ConfigParser

#Function getting the value belonging to a key
#returns: The value belonging to the key. I.e. prints it out (to the variable in the bash script)
def getValue(configfile,section,key):
	value=''
	config=ConfigParser.ConfigParser()
	config.read(configfile)
	try:
		value=config.get(section,key)
	except ConfigParser.NoOptionError as noe:
		value="InvOpt" #invalid Option
	except ConfigParser.NoSectionError as nse:
		value="InvSec" #invalid Section
	return value

#The main function of this script
def main():
	res=''
	if len(sys.argv)!=4:
		print("The function to retreive the value belonging to the key has been called with the wrong amount of arguments\n Please refer to the ConfigReader.py script to see how it is called")
		sys.exit(0)
	else:
		config_file=sys.argv[1]
		section=sys.argv[2]
		key=sys.argv[3]
		
		if os.path.isfile(config_file):
			res=getValue(config_file,section,key)			
		else:
			res="InvConf"	#invalid Configuration File

	print(str(res))
	


if __name__ == "__main__":
    main()


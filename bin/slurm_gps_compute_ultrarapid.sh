#!/bin/bash
#
# Script for calculating GPS positions using RTKLIB in 6 hr intervals
# Using IGS ultrarapid data products
#
# author: Jan Beutel
# (c) University of Innsbruck, 2021
#
#SBATCH --mail-type=FAIL                                # mail configuration: NONE, BEGIN, END, FAIL, REQUEUE, ALL
#SBATCH --mail-user=jan.beutel@uibk.ac.at                                                           
### SBATCH --output=/ifi-NAS/nes/research/slurm_logs/slurm_gps_compute_ultrarapid_%j.out   # where to store the output ( %j is the JOBID )
#SBATCH --output=/scratch/jan.beutel/slurm_logs/slurm_gps_compute_ultrarapid_%j.out # where to store the output ( %j is the JOBID )
#SBATCH -e /scratch/jan.beutel/slurm_logs/slurm_gps_compute_ultrarapid_%j.err
#SBATCH --mem=4G
#SBATCH --exclude=headnode
#SBATCH --cpus-per-task=1
### SBATCH --hint=multithread
#SBATCH --partition=IFIall

source ~/bin/conda/etc/profile.d/conda.sh
conda activate base

opt_dl=false  	  # bool indicating if the option -d is set
opt_ul=false  	  # bool for data upload to GSN
number_of_opts=0  #count of chosen options

while getopts "du" options; do
  case $options in
    d ) opt_dl=true
		number_of_opts=$((number_of_opts+1))
    ;;
    u ) opt_ul=true
		number_of_opts=$((number_of_opts+1))
    ;;
    \?) echo "Invalid arguments. Usage: compute_ultrarapid.sh -d -u YYYY DoY"
        exit 1
  esac
done

# if [ $# -eq 0 ]; then
#     echo "No arguments provided"
#     exit 1
# fi


#echo $number_of_opts
i=$((number_of_opts+1))
year=$(eval echo \${$i})
i=$((i+1))
DoYarg=$(eval echo \${$i})

if [ -z $year ]; then
  # echo "year is empty"
  year=$(date +%Y)
  month=$(date +%m)
  day=$(date +%d)
  printf -v mmonth "%02d" ${month#0}
  printf -v dday "%02d" ${day#0}
  #printf -v day_of_year "%03d" $day_of_year
  #Use the C library API to convert from DDMMYY to the number of seconds since the C epoch (1/1/1970), subtract the number of seconds until 5/1/1980 and divide the result by 7*24*3600 to get the number of weeks elapsed from 5th January, 1980.
  DoYarg=$(date -d $year-$mmonth-$dday +%j)
fi

if [ -z $DoYarg ]; then
  echo "DoYear is empty"
  exit 1
fi

doy=$DoYarg
echo Computing ultrarapid position for year $year, single DoY $doy
year=$(date -d "$year-01-01 +$DoYarg days -1 day" "+%Y")
month=$(date -d "$year-01-01 +$DoYarg days -1 day" "+%m")
day=$(date -d "$year-01-01 +$DoYarg days -1 day" "+%d")

# leap years 2012, 2016, 2020
if [ $doy == "366" ]; then
  if [[ $year =~ ^(2012|2016|2020|2024)$ ]]; then
    echo "$year is a leap year"
  else
    print "$year is not a leap year"
    exit 1
  fi
fi


# year=$(date +%Y)
# month=$(date +%m)
# day=$(date +%d)
# year=2021
# month=04
# day=05

# for j in {01..12}; do
# for i in {01..31}; do
#     # month=02
#     month=$(printf "%02d" $j)
#     day=$(printf "%02d" $i)

yyear=${year: -2}
# printf -v yyear "%02d" ${year#0}
printf -v mmonth "%02d" ${month#0}
printf -v dday "%02d" ${day#0}
#printf -v day_of_year "%03d" $day_of_year

#Use the C library API to convert from DDMMYY to the number of seconds since the C epoch (1/1/1970), subtract the number of seconds until 5/1/1980 and divide the result by 7*24*3600 to get the number of weeks elapsed from 5th January, 1980.
day_of_year=$(date -d $year-$mmonth-$dday +%j)
day_of_week=$(date -d $year-$mmonth-$dday +%w)
now=$(date -d $year-$mmonth-$dday +%s)

seconds_in_week=$((60*60*24*7)) #604800
leap_seconds=$((18)) 
gps_epoch=$(date -d 1980-1-6 +%s) # epoch start 5th January, 1980
gps_week=$(( (($now + leap_seconds) - $gps_epoch + 3600) / $seconds_in_week))


prefix_dir="/ifi-NAS/nes/research/gps"
gps_processing_dir=${prefix_dir}
gps_bin_dir=${gps_processing_dir}"/rtklib_processing/bin"
rtklib_bin_dir="/ifi-NAS/nes/research/gps/rtklib_processing/bin"
result_dir=${prefix_dir}"/rtklib_processing"
temp_dir="/tmp/temp_"$year$mmonth$dday

echo "#########################################################################"
echo "****** Executing GPS Compute UltraRapid *******"
echo "****** Starting on: `date`"
echo $year $yyear $mmonth $dday $day_of_year $gps_week $day_of_week
echo

if $opt_dl; then
    echo "****** Downloading IGS Data UltraRapid *******"
    echo
    cd ${gps_processing_dir}/external_dataproducts/igu_data
    ${gps_bin_dir}/download_igu_data.sh $year $day_of_year
fi


if ! [ -x "$temp_dir" ]
then
mkdir ${temp_dir}
echo "Temp directory created ${temp_dir}"
fi

echo "****** Now executing GPS Calculation *******"
for rover_station_label in DI03 BH13 DI04 DI07 LS05 LS11 LS12 GG01 GG02; do 

    echo $rover_station_label
    # prep variables from config file
    rover_station_nr=$(grep rover_station_nr ${gps_bin_dir}/config/parameter_file_${rover_station_label}.txt | cut -d ":" -f2 | xargs)
    rover_station_deployment=$(grep rover_station_deployment ${gps_bin_dir}/config/parameter_file_${rover_station_label}.txt | cut -d ":" -f2 | xargs)
    rover_gsn_upload_server=$(grep rover_gsn_upload_server ${gps_bin_dir}/config/parameter_file_${rover_station_label}.txt | cut -d ":" -f2 | xargs)
    rover_gsn_upload_port=$(grep rover_gsn_upload_port ${gps_bin_dir}/config/parameter_file_${rover_station_label}.txt | cut -d ":" -f2 | xargs)
    base_station_label=$(grep base_station_label ${gps_bin_dir}/config/parameter_file_${rover_station_label}.txt | cut -d ":" -f2 | xargs)
    
    # grabenguferstation="GG02"
    # if [ "$station" == "$grabenguferstation" ]; then
    #     base_station_label="RG01"
    # else
    #     base_station_label="RD01"
    # fi
    #echo $station $base_station_label

    #Dummy execution to download obs data, prepare config file in /tmp
    cd ${gps_processing_dir}/rtklib_processing/bin
    ./prepare_ultrarapid.sh -p config/parameter_file_${rover_station_label}.txt $year $mmonth $dday

    # Preparing data
    # cp -r ${temp_dir}/temp_$year$mmonth$dday ${prefix_dir}
    cd ${temp_dir}
    cp ${gps_processing_dir}/rtklib_processing/bin/config/rtkpost_static_${base_station_label}_fast.conf .
    cp ${gps_processing_dir}/external_dataproducts/igu_data/zim2${day_of_year}* .
    cp ${gps_processing_dir}/external_dataproducts/igu_data/igv$gps_week$day_of_week*.sp3 .
    cp ${gps_processing_dir}/external_dataproducts/igu_data/igu$gps_week$day_of_week*.sp3 .
    cp ${gps_processing_dir}/external_dataproducts/igu_data/igu$gps_week$day_of_week*.erp .
    
    #Calling RNX2RTKP
    if [ -f ${rover_station_label}${day_of_year}0.${yyear}O ]; then

        # echo "${rtklib_bin_dir}/rnx2rtkp -x 0 -k ${gps_bin_dir}/config/rtkpost_static_${base_station_label}_fast.conf -o ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_$year${mmonth}${dday}_00.pos -p 3 -c ${rover_station_label}${day_of_year}0.${yyear}O ${base_station_label}${day_of_year}0.${yyear}O igu$gps_week${day_of_week}_00.erp igu$gps_week${day_of_week}_00.sp3 igv$gps_week${day_of_week}_00.sp3 zim2${day_of_year}{a..f}.${yyear}n -ts $year/$mmonth/$dday 00:00:00 -te $year/$mmonth/$dday 06:00:00"
        echo "${rtklib_bin_dir}/rnx2rtkp -x 0 -k ${gps_bin_dir}/config/rtkpost_static_${base_station_label}_fast.conf -o ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_$year${mmonth}${dday}_00.pos -p 3 -c ${rover_station_label}${day_of_year}0.${yyear}O ${base_station_label}${day_of_year}0.${yyear}O igu$gps_week${day_of_week}_00.erp igu$gps_week${day_of_week}_00.sp3 zim2${day_of_year}{a..f}.${yyear}n -ts $year/$mmonth/$dday 00:00:00 -te $year/$mmonth/$dday 06:00:00"
        echo
        if [ -f igu$gps_week${day_of_week}_00.sp3 ]; then
            #${rtklib_bin_dir}/rnx2rtkp -x 0 -k ${gps_bin_dir}/config/rtkpost_static_${base_station_label}_fast.conf -o ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_$year${mmonth}${dday}_00.pos -p 3 -c ${rover_station_label}${day_of_year}0.${yyear}O ${base_station_label}${day_of_year}0.${yyear}O igu$gps_week${day_of_week}_00.erp igu$gps_week${day_of_week}_00.sp3 igv$gps_week${day_of_week}_00.sp3 zim2${day_of_year}{a..f}.${yyear}n -ts $year/$mmonth/$dday 00:00:00 -te $year/$mmonth/$dday 06:00:00
            ${rtklib_bin_dir}/rnx2rtkp -x 0 -k ${gps_bin_dir}/config/rtkpost_static_${base_station_label}_fast.conf -o ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_$year${mmonth}${dday}_00.pos -p 3 -c ${rover_station_label}${day_of_year}0.${yyear}O ${base_station_label}${day_of_year}0.${yyear}O igu$gps_week${day_of_week}_00.erp igu$gps_week${day_of_week}_00.sp3 zim2${day_of_year}{a..f}.${yyear}n -ts $year/$mmonth/$dday 00:00:00 -te $year/$mmonth/$dday 06:00:00
            #  >/dev/null 2>&1
            # echo
        fi
        if [ -f igu$gps_week${day_of_week}_06.sp3 ]; then
            # ${rtklib_bin_dir}/rnx2rtkp -x 0 -k ${gps_bin_dir}/config/rtkpost_static_${base_station_label}_fast.conf -o ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_$year${mmonth}${dday}_06.pos -p 3 -c ${rover_station_label}${day_of_year}0.${yyear}O ${base_station_label}${day_of_year}0.${yyear}O igu$gps_week${day_of_week}_06.erp igu$gps_week${day_of_week}_06.sp3 igv$gps_week${day_of_week}_06.sp3 zim2${day_of_year}{g..l}.${yyear}n -ts $year/$mmonth/$dday 06:00:00 -te $year/$mmonth/$dday 12:00:00
            ${rtklib_bin_dir}/rnx2rtkp -x 0 -k ${gps_bin_dir}/config/rtkpost_static_${base_station_label}_fast.conf -o ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_$year${mmonth}${dday}_06.pos -p 3 -c ${rover_station_label}${day_of_year}0.${yyear}O ${base_station_label}${day_of_year}0.${yyear}O igu$gps_week${day_of_week}_06.erp igu$gps_week${day_of_week}_06.sp3 zim2${day_of_year}{g..l}.${yyear}n -ts $year/$mmonth/$dday 06:00:00 -te $year/$mmonth/$dday 12:00:00
            #  >/dev/null 2>&1
            # echo
        fi
        if [ -f igu$gps_week${day_of_week}_12.sp3 ]; then
            # ${rtklib_bin_dir}/rnx2rtkp -x 0 -k ${gps_bin_dir}/config/rtkpost_static_${base_station_label}_fast.conf -o ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_$year${mmonth}${dday}_12.pos -p 3 -c ${rover_station_label}${day_of_year}0.${yyear}O ${base_station_label}${day_of_year}0.${yyear}O igu$gps_week${day_of_week}_12.erp igu$gps_week${day_of_week}_12.sp3 igv$gps_week${day_of_week}_12.sp3 zim2${day_of_year}{m..r}.${yyear}n -ts $year/$mmonth/$dday 12:00:00 -te $year/$mmonth/$dday 18:00:00
            ${rtklib_bin_dir}/rnx2rtkp -x 0 -k ${gps_bin_dir}/config/rtkpost_static_${base_station_label}_fast.conf -o ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_$year${mmonth}${dday}_12.pos -p 3 -c ${rover_station_label}${day_of_year}0.${yyear}O ${base_station_label}${day_of_year}0.${yyear}O igu$gps_week${day_of_week}_12.erp igu$gps_week${day_of_week}_12.sp3 zim2${day_of_year}{m..r}.${yyear}n -ts $year/$mmonth/$dday 12:00:00 -te $year/$mmonth/$dday 18:00:00
            # >/dev/null 2>&1
            # echo
        fi
        if [ -f igu$gps_week${day_of_week}_18.sp3 ]; then
            # ${rtklib_bin_dir}/rnx2rtkp -x 0 -k ${gps_bin_dir}/config/rtkpost_static_${base_station_label}_fast.conf -o ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_$year${mmonth}${dday}_18.pos -p 3 -c ${rover_station_label}${day_of_year}0.${yyear}O ${base_station_label}${day_of_year}0.${yyear}O igu$gps_week${day_of_week}_18.erp igu$gps_week${day_of_week}_18.sp3 igv$gps_week${day_of_week}_18.sp3 zim2${day_of_year}{s..x}.${yyear}n -ts $year/$mmonth/$dday 18:00:00 -te $year/$mmonth/$dday 24:00:00
            ${rtklib_bin_dir}/rnx2rtkp -x 0 -k ${gps_bin_dir}/config/rtkpost_static_${base_station_label}_fast.conf -o ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_$year${mmonth}${dday}_18.pos -p 3 -c ${rover_station_label}${day_of_year}0.${yyear}O ${base_station_label}${day_of_year}0.${yyear}O igu$gps_week${day_of_week}_18.erp igu$gps_week${day_of_week}_18.sp3 zim2${day_of_year}{s..x}.${yyear}n -ts $year/$mmonth/$dday 18:00:00 -te $year/$mmonth/$dday 24:00:00
            # >/dev/null 2>&1
            # echo
        fi
        
        # Cleaning up
        rm -f ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/*events.pos

        cd ${gps_processing_dir}/rtklib_processing/bin/
        
        upload_done=false

        for quartertime in 00 06 12 18; do
            result_file=${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_$year${mmonth}${dday}_${quartertime}.pos
            # echo $result_file
            if test -f "$result_file"; then
                lastline=$(tail -q -n 1 $result_file)
                #echo $lastline
                if [[ $lastline != *"GPST"* ]]; then
                    head -n -1 ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_$year${mmonth}${dday}_${quartertime}.pos > ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_lv95_$year${mmonth}${dday}_${quartertime}.pos
                    lv95_gsn_upload_string='lv95_convert_solution.py '${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_$year${mmonth}${dday}_${quartertime}.pos''
                    python $lv95_gsn_upload_string >> ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_lv95_$year${mmonth}${dday}_${quartertime}.pos
                    # echo ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_lv95_$year${mmonth}${dday}_${quartertime}.pos
                    tail -q -n1 ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_lv95_$year${mmonth}${dday}_${quartertime}.pos
                fi
            fi
        done
        echo "Done converting"

        for quartertime in 00 06 12 18; do
            if $opt_ul; then
                result_file=${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_$year${mmonth}${dday}_${quartertime}.pos
                # echo $result_file
                if test -f "$result_file"; then
                    lastline=$(tail -q -n 1 $result_file)
                    #echo $lastline
                    if [[ $lastline != *"GPST"* ]]; then
                        ## ssh forward to data.permasense.ch typically 22501
                        port_no=$(comm -23 <(seq 49152 60535 | sort) <(ss -Htan | awk '{print $4}' | cut -d':' -f2 | sort -u) | shuf | head -n 1)
                        echo "Uploading data for rover ${rover_station_label} position ${rover_station_nr} to ${rover_gsn_upload_server} port ${rover_gsn_upload_port} via ssh tunnel on port $port_no"
                        ssh -f -o ExitOnForwardFailure=yes -L ${port_no}:127.0.0.1:${rover_gsn_upload_port} jbeutel@data.permasense.ch sleep 5

                        # upload to GSN
                        # 0 - bernese, 1 - rtklib rt, 2 - rtklib batch, 3 - rtklib6h
                        lv95_gsn_upload_string='lv95_convert_solution.py '${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_$year${mmonth}${dday}_${quartertime}.pos' '$rover_station_nr' '${rover_station_label}' '${base_station_label}' '3''
                        # echo $lv95_gsn_upload_string
                        python $lv95_gsn_upload_string  | nc -q1 -w1 127.0.0.1 $port_no 
                        # nc $rover_gsn_upload_server $rover_gsn_upload_port
                        upload_done=true
                    fi
                fi
            fi
        done
        if $upload_done; then
            echo "Upload successful."
        fi

        # ls -al ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/
        #   tail -q -n1 ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/${rover_station_label}00CHE_lv95_$year${mmonth}${dday}_*.pos
        # write a consolidated pos file
        tail -q -n1 ${result_dir}/${rover_station_deployment}/${rover_station_label}/results_6h/*lv95*.pos > ${result_dir}/${rover_station_deployment}/${rover_station_label}00CHE_fast.pos
        # cp ${result_dir}/${rover_station_deployment}/${rover_station_label}00CHE_fast.pos ~/public_html/${rover_station_label}00CHE_fast.pos
        # chmod 664 ~/public_html/${rover_station_label}00CHE_fast.pos
    fi
    echo
done

# done
# done

# discard ubx files -> they are deleted together with the temp_dir
rm -r $temp_dir
echo "Deletes the temporary folder $temp_dir."
echo "****** Done. Go throw some rocks. ******"

'''
Script returning the position- and date-depending name of the gps raw data file.
'''

from Stringbuilder import Stringbuilder
import DateConverter
import sys

# Function returning the date- and position-depending name of the gps raw data (ubx) file 
# arguments:	label: the station' label
#				year, month, day: the date
def get_ubx_fileName(label, year, month, day):
	year=int(year)
	xYear=year%100
	month.lstrip('0') 
	month=int(month)
	day.lstrip('0') 
	day=int(day)
	dayOfYear=DateConverter.dayOfYear(year,month,day)
	xdayOfYear=str('%03.0f' %dayOfYear)
	filename=Stringbuilder(str(label))
	filename.append(str(xdayOfYear))
	filename.append("0")
	filename.append(".ubx")
	return filename.str()

# This script's main funtion
def main():
	if len(sys.argv)!=5:
		sys.exit("GetObsFileName.py must be called with 4 arguments")
	else:
		label=sys.argv[1]
		year=sys.argv[2]
		month=sys.argv[3]
		day=sys.argv[4]
		r=get_ubx_fileName(label,year,month,day)
		print(r)

if __name__ == "__main__":
   main()



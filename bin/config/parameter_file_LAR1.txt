#Configuration file with parameters used in order to download and convert data

[positions]
rover_station_deployment: permos
rover_gsn_server:         data.permasense.ch
rover_station_nr:         08
rover_station_label:      LAR1
rover_start_date:         26.09.2014
rover_gsn_upload_server:  data.permasense.ch
rover_gsn_upload_port:    22502

base_station_deployment:  pnac
base_gsn_server:
base_station_nr:
base_station_label:       SANB
ref_pos_x:                4345821.0440
ref_pos_y:                702666.5120
ref_pos_z:                4602145.7270

[servers]
#clk, sp3 and erp files
dataserver_1=ftp://igs.ensg.ign.fr/pub/igs/products
#nav files
dataserver_2=ftp://igs.ensg.ign.fr/pub/igs/data
#dcb files
dataserver_3=ftp://ftp.aiub.unibe.ch/CODE
#iono files
dataserver_4=ftp://igs.ensg.ign.fr/pub/igs/products/ionosphere

[directories]
igs_data_dir:           /ifi-NAS/nes/research/gps/external_dataproducts/igs_data
igr_data_dir:           /ifi-NAS/nes/research/gps/external_dataproducts/igr_data
code_data_dir:          /ifi-NAS/nes/research/gps/external_dataproducts/code_data
rtklib_dir:             /ifi-NAS/nes/research/gps/tools/rtklib_rtklibexplorer_github/bin
rtklib_options_dir:     /ifi-NAS/nes/research/gps/rtklib_processing/bin/config
gps_data_dir:           /ifi-NAS/nes/research/gps/rtklib_processing
output_dir:             /ifi-NAS/nes/research/gps/rtklib_processing

[files]
rtklib_conf_file:       rtkpost_static_SANB.conf

#Configuration file with parameters used in order to download and convert data

[positions]
rover_station_deployment: matterhorn
rover_gsn_server:         data.permasense.ch
rover_station_nr:         40
rover_station_label:      MH40
rover_start_date:         03.06.2015
rover_gsn_upload_server:  data.permasense.ch
rover_gsn_upload_port:    22503

base_station_deployment:  cogear
base_gsn_server:
base_station_nr:          42
base_station_label:       HOGR
ref_pos_x:                4402512.7848
ref_pos_y:                592979.9134
ref_pos_z:                4566317.6154

[servers]
#clk, sp3 and erp files
dataserver_1=ftp://gssc.esa.int/igs/products
#nav files
dataserver_2=ftp://gssc.esa.int/igs/data/daily
#dcb files
dataserver_3=ftp://ftp.aiub.unibe.ch/CODE
#iono files
dataserver_4=ftp://gssc.esa.int/igs/products/ionex

[directories]
igs_data_dir:           /ifi-NAS/nes/research/gps/external_dataproducts/igs_data
igr_data_dir:           /ifi-NAS/nes/research/gps/external_dataproducts/igr_data
code_data_dir:          /ifi-NAS/nes/research/gps/external_dataproducts/code_data
rtklib_dir:             /ifi-NAS/nes/research/gps/rtklib_processing/bin
rtklib_options_dir:     /ifi-NAS/nes/research/gps/rtklib_processing/bin/config
gps_data_dir:           /ifi-NAS/nes/research/gps/rtklib_processing
output_dir:             /ifi-NAS/nes/research/gps/rtklib_processing

[files]
rtklib_conf_file:       rtkpost_static_HOGR.conf

#Configuration file with parameters used in order to download and convert data

[positions]
rover_station_deployment: dirruhorn
rover_gsn_server:         data.permasense.ch
rover_station_nr:         43
rover_station_label:      GG01
rover_start_date:         17.08.2011
rover_gsn_upload_server:  data.permasense.ch
rover_gsn_upload_port:    22501

base_station_deployment:  dirruhorn
base_gsn_server:          data.permasense.ch
base_station_nr:          39
base_station_label:       RG01
ref_pos_x:                4392040.8940
ref_pos_y:                602680.4440
ref_pos_z:                4574388.7730

[servers]
#clk, sp3 and erp files
dataserver_1=ftp://igs.ensg.ign.fr/pub/igs/products
#nav files
dataserver_2=ftp://igs.ensg.ign.fr/pub/igs/data
#dcb files
dataserver_3=ftp://ftp.aiub.unibe.ch/CODE
#iono files
dataserver_4=ftp://igs.ensg.ign.fr/pub/igs/products/ionosphere

[directories]
igs_data_dir:           /ifi-NAS/nes/research/gps/external_dataproducts/igs_data
igr_data_dir:           /ifi-NAS/nes/research/gps/external_dataproducts/igr_data
code_data_dir:          /ifi-NAS/nes/research/gps/external_dataproducts/code_data
rtklib_dir:             /ifi-NAS/nes/research/gps/tools/rtklib_rtklibexplorer_github/bin
rtklib_options_dir:     /ifi-NAS/nes/research/gps/rtklib_processing/bin/config
gps_data_dir:           /ifi-NAS/nes/research/gps/rtklib_processing
output_dir:             /ifi-NAS/nes/research/gps/rtklib_processing

[files]
rtklib_conf_file:       rtkpost_static_RG01.conf

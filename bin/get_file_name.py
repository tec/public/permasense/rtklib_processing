#!/usr/bin/env python
'''
Collection of functions returning specific (date-depending) file and folder names
'''
from Stringbuilder import Stringbuilder
import DateConverter
import sys

# function returning the date-depending name of the navigation file
def get_nav_fileName(year, month, day):
	xYear=year%100
	dayOfYear=DateConverter.dayOfYear(year,month,day)
	xdayOfYear=str('%03.0f' %dayOfYear)
	filename=Stringbuilder('')
	filename.append("brdc")
	filename.append(xdayOfYear)
	filename.append("0")
	filename.append("."+str(xYear)+"n.Z")
	return filename.str()

# function returning the date-depending name of the navigation file
def get_navu_fileName(year, month, day):
	xYear=year%100
	dayOfYear=DateConverter.dayOfYear(year,month,day)
	xdayOfYear=str('%03.0f' %dayOfYear)
	filename=Stringbuilder('')
	filename.append("zim2")
	filename.append(xdayOfYear)
	filename.append("u")
	filename.append("."+str(xYear)+"n.Z")
	return filename.str()

# function returning the date-depending part of the path leading to the navigation data file
def get_nav_folderName(year, month, day):
	xYear=year%100
	dayOfYear=DateConverter.dayOfYear(year,month,day)
	xdayOfYear=str('%03.0f' %dayOfYear)
	# filename=Stringbuilder("/"+str(year)+"/"+str('%03.0f' %dayOfYear)+"/"+str(xYear)+"n/")
	filename=Stringbuilder("/"+str(year)+"/"+str('%03.0f' %dayOfYear)+"/")
	#filename.append("brdc")
	#filename.append(xdayOfYear)
	#filename.append("0")
	#filename.append("."+str(xYear)+"n.Z")
	return filename.str()

# function returning the date-depending part of the path leading to the navigation data file
def get_navu_folderName(year, month, day):
	xYear=year%100
	dayOfYear=DateConverter.dayOfYear(year,month,day)
	xdayOfYear=str('%03.0f' %dayOfYear)
	filename=Stringbuilder("/"+str(year)+"/"+str('%03.0f' %dayOfYear)+"/"+str(xYear) + "/")
	#filename.append("brdc")
	#filename.append(xdayOfYear)
	#filename.append("0")
	#filename.append("."+str(xYear)+"n.Z")
	return filename.str()

# function returning the date-depending name of the dcb file
def get_dcb_fileName(year,month):
	xYear=year%100
#	filename=Stringbuilder("P1P2_ALL")
#	filename=Stringbuilder("")
	filename=Stringbuilder("P1P2")
	filename.append(str(xYear))
	filename.append(str('%02.0f' %month))
	filename.append(".DCB.Z")
	return filename.str()

# function returning the date-depending part of the path leading to the differential code bias data file
def get_dcb_folderName(year,month):
	xYear=year%100
	filename=Stringbuilder("/"+str(year)+"/")
#	filename=Stringbuilder("/")
#	filename.append("P1P2_ALL")
	return filename.str()

# function returning the date-depending name of the sp3 file
def get_sp3_fileName(year, month, day):
	gpsWeek=DateConverter.gpsWeek(year, month, day)
	dayOfWeek=DateConverter.dayOfWeek(year, month, day)
	filename=Stringbuilder('')
	filename.append("igs")
	filename.append(str(gpsWeek))
	filename.append(str(dayOfWeek))
	filename.append(".sp3.Z")
	return filename.str()

# function returning the date-depending name of the sp3 file
def get_sp3_rapid_fileName(year, month, day):
	gpsWeek=DateConverter.gpsWeek(year, month, day)
	dayOfWeek=DateConverter.dayOfWeek(year, month, day)
	filename=Stringbuilder('')
	filename.append("igr")
	filename.append(str(gpsWeek))
	filename.append(str(dayOfWeek))
	filename.append(".sp3.Z")
	return filename.str()

# function returning the date-depending part of the path leading to the sp3 file
def get_sp3_folderName(year, month, day):
	gpsWeek=DateConverter.gpsWeek(year, month, day)
	dayOfWeek=DateConverter.dayOfWeek(year, month, day)
	filename=Stringbuilder("/"+str(gpsWeek)+"/")
	#filename.append("igs")
	#filename.append(str(gpsWeek))
	#filename.append(str(dayOfWeek))
	#filename.append(".sp3.Z")
	return filename.str()

# function returning the date-depending name of the clk file
def get_clk_fileName(year, month, day):
	gpsWeek=DateConverter.gpsWeek(year, month, day)
	dayOfWeek=DateConverter.dayOfWeek(year, month, day)
	filename=Stringbuilder('')
	filename.append("igs")
	filename.append(str(gpsWeek))
	filename.append(str(dayOfWeek))
	filename.append(".clk_30s.Z")
	return filename.str()

# function returning the date-depending name of the clk file
def get_clk_rapid_fileName(year, month, day):
	gpsWeek=DateConverter.gpsWeek(year, month, day)
	dayOfWeek=DateConverter.dayOfWeek(year, month, day)
	filename=Stringbuilder('')
	filename.append("igr")
	filename.append(str(gpsWeek))
	filename.append(str(dayOfWeek))
	filename.append(".clk.Z")
	return filename.str()

# function returning the date-depending part of the path leading to the clk file
def get_clk_folderName(year, month, day):
	gpsWeek=DateConverter.gpsWeek(year, month, day)
	dayOfWeek=DateConverter.dayOfWeek(year, month, day)
	filename=Stringbuilder("/"+str(gpsWeek)+"/")
	#filename.append("igs")
	#filename.append(str(gpsWeek))
	#filename.append(str(dayOfWeek))
	#filename.append(".clk_30s.Z")
	return filename.str()

# function returning the date-depending name of the iono file
def get_iono_fileName(year, month, day):
	xYear=year%100
	dayOfYear=DateConverter.dayOfYear(year,month,day)
	xdayOfYear=str('%03.0f' %dayOfYear)
	filename=Stringbuilder('')
	filename.append("igsg")
	filename.append(xdayOfYear)
	filename.append("0")
	filename.append("."+str(xYear)+"i.Z")
	return filename.str()

# function returning the date-depending name of the iono file
def get_iono_rapid_fileName(year, month, day):
	xYear=year%100
	dayOfYear=DateConverter.dayOfYear(year,month,day)
	xdayOfYear=str('%03.0f' %dayOfYear)
	filename=Stringbuilder('')
	filename.append("igrg")
	filename.append(xdayOfYear)
	filename.append("0")
	filename.append("."+str(xYear)+"i.Z")
	return filename.str()

# function returning the date-depending part of the path leading to the iono data file
def get_iono_folderName(year, month, day):
	xYear=year%100
	dayOfYear=DateConverter.dayOfYear(year,month,day)
	xdayOfYear=str('%03.0f' %dayOfYear)
	filename=Stringbuilder("/"+str(year)+"/"+str('%03.0f' %dayOfYear)+"/")
	#filename.append("igsg")
	#filename.append(xdayOfYear)
	#filename.append("0")
	#filename.append("."+str(xYear)+"i.Z")
	return filename.str()

# function returning the date-depending name of the eopfile
def get_erp_fileName(year, month, day):
	gpsWeek=DateConverter.gpsWeek(year, month, day)
	dayOfWeek=DateConverter.dayOfWeek(year, month, day)
	filename=Stringbuilder('')
	filename.append("igs")
	filename.append(str(gpsWeek))
	filename.append("7")
	filename.append(".erp.Z")
	return filename.str()

# function returning the date-depending name of the eopfile
def get_erp_rapid_fileName(year, month, day):
	gpsWeek=DateConverter.gpsWeek(year, month, day)
	dayOfWeek=DateConverter.dayOfWeek(year, month, day)
	filename=Stringbuilder('')
	filename.append("igr")
	filename.append(str(gpsWeek))
	filename.append(str(dayOfWeek))
	filename.append(".erp.Z")
	return filename.str()

# function returning the date-depending part of the path leading to the eop file
def get_erp_folderName(year, month, day):
	gpsWeek=DateConverter.gpsWeek(year, month, day)
	dayOfWeek=DateConverter.dayOfWeek(year, month, day)
	filename=Stringbuilder("/"+str(gpsWeek)+"/")
	#filename.append("igs")
	#filename.append(str(gpsWeek))
	#filename.append("7")
	#filename.append(".erp.Z")
	return filename.str()

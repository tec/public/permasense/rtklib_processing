#!/bin/bash
#
# slurm_gps_compute.sh job-script for running RTKLIB on SLURM using sbatch
#
# slurm_gps_compute.sh -f -u -d YYYY DoY
#     -f: use IGS final data product
#     -u: upload to GSN database
#     -d: igs data download

#
# author: Jan Beutel
# (c) University of Innsbruck, 2021
#
#SBATCH --mail-type=FAIL                                # mail configuration: NONE, BEGIN, END, FAIL, REQUEUE, ALL
#SBATCH --mail-user=jan.beutel@uibk.ac.at
### SBATCH --output=/ifi-NAS/nes/research/slurm_logs/slurm_gps_compute_%j.log   # where to store the output ( %j is the JOBID )
#SBATCH --output=/scratch/jan.beutel/slurm_logs/slurm_gps_compute_%j.log # where to store the output ( %j is the JOBID )
#SBATCH -e /scratch/jan.beutel/slurm_logs/slurm_gps_compute_%j.err
#SBATCH --mem=4G
#SBATCH --exclude=headnode,gc17
#SBATCH --cpus-per-task=2
#SBATCH --hint=multithread
#SBATCH --partition=IFIall


# for n in {2011..2022}; do for m in {001..366}; do sbatch ./slurm_gps_compute_1h.sh -f $n $m; done; done

# for n in {2011..2022}; do tail -n1 -q GG01/results_1h/*_${n}*.pos; done


gps_processing_dir="/ifi-NAS/nes/research/gps"
# gps_processing_dir="/scratch/jan.beutel/research/gps"
gps_bin_dir=${gps_processing_dir}"/rtklib_processing/bin"
external_dataproducts_dir=${gps_processing_dir}"/external_dataproducts"
# rtklib_bin_dir="/ifi-NAS/nes/research/gps/tools/rtklib_rtklibexplorer_github/bin"
# result_dir=${gps_processing_dir}"/rtklib_processing"
# temp_dir="/tmp/temp_"$year$mmonth$dday

### export LD_LIBRARY_PATH=/ifi-NAS/nes/research/gps/tools/oneapi/mkl/latest/lib/intel64

/bin/echo SLURM Running on host: `hostname`
/bin/echo SLURM executing in directory: `pwd`
/bin/echo SLURM Starting on: `date`
/bin/echo SLURM_JOB_ID: $SLURM_JOB_ID
# /bin/echo SLURM - In directory: ${gps_bin_dir}

cd ${gps_bin_dir}
source ~/bin/conda/etc/profile.d/conda.sh
conda activate base

#
# binary to execute
#/bin/echo Input: $1 $2    YEAR DoY
#

opt_final=false		    #bool indicating if the option -f is set
opt_upload=false		  #bool indicating if the option -u is set
opt_download=false		#bool indicating if the option -d is set
number_of_opts=0	    #count of chosen options
final_option=""

while getopts "fud" options; do
  case $options in
    f ) opt_final=true
    final_option="-f "
    #echo $final_option
    number_of_opts=$((number_of_opts+1))
    #echo $number_of_opts
    ;;
    u ) opt_upload=true
    upload_option="-u "
    number_of_opts=$((number_of_opts+1))
    #echo $number_of_opts
    ;;
    d ) opt_download=true
    download_option="-d "
    number_of_opts=$((number_of_opts+1))
    #echo $number_of_opts
    ;;
	  \?) echo "Invalid arguments. Usage: slurm_gps_compute.sh -f -u -d YYYY DoY"
    	exit 1
  esac
done

#echo $number_of_opts
i=$((number_of_opts+1))
year=$(eval echo \${$i})
#echo $year
i=$((i+1))
DoYarg=$(eval echo \${$i})
#echo $DoYarg

doy=$DoYarg
/bin/echo SLURM - Computing position for year $year, single DoY $doy
year=$(date -d "$year-01-01 +$DoYarg days -1 day" "+%Y")
month=$(date -d "$year-01-01 +$DoYarg days -1 day" "+%m")
day=$(date -d "$year-01-01 +$DoYarg days -1 day" "+%d")

# leap years 2012, 2016, 2020
if [ $doy == "366" ]; then
  if [[ $year =~ ^(2012|2016|2020|2024)$ ]]; then
    echo "$year is a leap year"
  else
    print "$year is not a leap year"
    exit 1
  fi
fi

# /bin/echo SLURM - Computing position for: $year $month $day DoY $doy
#			-d: igs data download
#			-b: no data download and no conversion for the basestation
#			-r: no data download and no conversion for the roverstation
#           -c: no conversion
# echo $opt_download
if $opt_download; then
  # echo "Downloading"
  if $opt_final
  then
    # echo "Final"
    cd ${external_dataproducts_dir}/igs_data
    ${gps_bin_dir}/download_igs_data.sh $year $doy
  else
    # echo "Rapid"
    cd ${external_dataproducts_dir}/igr_data
    ${gps_bin_dir}/download_igr_data.sh $year $doy
  fi
fi

cd ${gps_bin_dir}

# execute Matterhorn, download IGS data
# start 2011 02 02 033
#./compute_solution.sh -p config/parameter_file_HOGR_ZERM.txt -b -r -c $download_option $final_option $upload_option $year $month $day
# echo ./compute_solution.sh -p config/parameter_file_HOGR_ZERM.txt -b -r -c $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_HOGR_ZERM.txt -b -r -c $final_option $upload_option $year $month $day

# # start 2014 08 14
# # ./compute_solution.sh -p config/parameter_file_MH33_ZERM.txt -b $final_option $upload_option $year $month $day
# # ./compute_solution.sh -p config/parameter_file_MH34_ZERM.txt -b $final_option $upload_option $year $month $day
# # # start 2015 06 02 to 2019 03 20 ??
# # ./compute_solution.sh -p config/parameter_file_MH35_ZERM.txt -b $final_option $upload_option $year $month $day
# # ./compute_solution.sh -p config/parameter_file_MH40_ZERM.txt -b $final_option $upload_option $year $month $day
# # ./compute_solution.sh -p config/parameter_file_MH43_ZERM.txt -b $final_option $upload_option $year $month $day

# # # old HOGR moving reference calculation
# # start 2014 08 14
# ./compute_solution.sh -p config/parameter_file_MH33.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_MH34.txt -b $final_option $upload_option $year $month $day
# # start 2015 06 02 to 2019 03 20 ??
# ./compute_solution.sh -p config/parameter_file_MH35.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_MH40.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_MH43.txt -b $final_option $upload_option $year $month $day
# # 2018 11 08 313 to 2019 03 19 079 HOGR not functional, MH33 was logger only, MH35 not working
# HOGR_stop_date=$(date -d "2018-11-09" '+%Y%m%d')
# HOGR_restart_date=$(date -d "2019-03-19" '+%Y%m%d')
# solution_date=$(date -d "$year-$month-$day" '+%Y%m%d')
# if [[ $solution_date -ge $HOGR_stop_date ]] && [[ $solution_date -le $HOGR_restart_date ]]; then
#   ./compute_solution.sh -p config/parameter_file_MH33_ZERM.txt -b -r $final_option $upload_option $year $month $day
#   ./compute_solution.sh -p config/parameter_file_MH34_ZERM.txt -b $final_option $upload_option $year $month $day
#   ./compute_solution.sh -p config/parameter_file_MH35_ZERM.txt -b $final_option $upload_option $year $month $day
#   ./compute_solution.sh -p config/parameter_file_MH40_ZERM.txt -b $final_option $upload_option $year $month $day
#   ./compute_solution.sh -p config/parameter_file_MH43_ZERM.txt -b $final_option $upload_option $year $month $day
# fi

# # execute Saastal starting 16.10.2014
# ##./compute_solution.sh -p config/parameter_file_KREB_ZERM.txt -b -r -c $download_option $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_KREB_ZERM.txt -b -r -c $final_option $upload_option $year $month $day

# ./compute_solution.sh -p config/parameter_file_HS01.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_WM01.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_WM02.txt -b $final_option $upload_option $year $month $day

# # execute Randa starting 2011-05-26 146
# ##./compute_solution.sh -p config/parameter_file_RAND_ZERM.txt -b -r -c $download_option $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_RAND_ZERM.txt -b -r -c $final_option $upload_option $year $month $day

# ./compute_solution.sh -p config/parameter_file_RA01.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_RA02.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_RA03.txt -b $final_option $upload_option $year $month $day

# ./compute_solution.sh -p config/parameter_file_WYS1.txt -b $final_option $upload_option $year $month $day
# # WYS logger only 2018-10-17 to 2019-03-26
# WYS1_stop_date=$(date -d "2018-10-17" '+%Y%m%d')
# WYS1_restart_date=$(date -d "2010-03-26" '+%Y%m%d')
# solution_date=$(date -d "$year-$month-$day" '+%Y%m%d')
# if [[ ${solution_date} -gt ${BH13_stop_date} ]] && [[ ${solution_date} -lt ${BH13_restart_date} ]]; then
#   ./compute_solution.sh -p config/parameter_file_WYS1.txt -b -r -c $final_option $upload_option $year $month $day
# fi

# # execute Dirruhorn
# ./compute_solution.sh -p config/parameter_file_RD01_ZERM.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_RD01_RAND.txt -b $final_option $upload_option $year $month $day
# ## ./compute_solution.sh -p config/parameter_file_RD02_RAND.txt -b  $year $month $day
# ## ./compute_solution.sh -p config/parameter_file_RD03_RAND.txt -b  $year $month $day
# ## ./compute_solution.sh -p config/parameter_file_RD04_RAND.txt -b  $year $month $day
# ## ./compute_solution.sh -p config/parameter_file_RD05_RAND.txt -b  $year $month $day
# ./compute_solution.sh -p config/parameter_file_RL01.txt -b  $final_option $upload_option $year $month $day

# ./compute_solution.sh -p config/parameter_file_BH03.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_BH07.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_BH09.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_BH10.txt -b $final_option $upload_option $year $month $day
# # BH12 has errors in RINEX data from 2013-06-26 to 2014-06-05
# BH12_stop_date=$(date -d "2013-06-26" '+%Y%m%d')
# BH12_restart_date=$(date -d "2014-06-05" '+%Y%m%d')
# solution_date=$(date -d "$year-$month-$day" '+%Y%m%d')
# if [[ $solution_date -gt $BH12_stop_date ]] && [[ $solution_date -lt $BH12_restart_date ]]; then
#   echo "skip BH12 errors"
# else
#   ./compute_solution.sh -p config/parameter_file_BH12.txt -b $final_option $upload_option $year $month $day
# fi

# ./compute_solution.sh -p config/parameter_file_BH13.txt -b $final_option $upload_option $year $month $day
# # BH13 logger only 2018-09-17 to 2020-04-03
# BH13_stop_date=$(date -d "2018-09-17" '+%Y%m%d')
# BH13_restart_date=$(date -d "2020-04-03" '+%Y%m%d')
# solution_date=$(date -d "$year-$month-$day" '+%Y%m%d')
# if [[ $solution_date -gt $BH13_stop_date ]] && [[ $solution_date -lt $BH13_restart_date ]]; then
#   ./compute_solution.sh -p config/parameter_file_BH13.txt -b -r -c $final_option $upload_option $year $month $day
# fi

# # RD01 not functional before 2011-03-01 and cycle slips in rover RINEX data 15-12-2010 to 01-05-2011
# DI_no_cycle_slips=$(date -d "2011-05-02" '+%Y%m%d')
# solution_date=$(date -d "$year-$month-$day" '+%Y%m%d')
# if [[ $solution_date -lt $DI_no_cycle_slips ]]; then
#     ./compute_solution.sh -p config/parameter_file_DI02_ZERM.txt -b $final_option $upload_option $year $month $day
#     ./compute_solution.sh -p config/parameter_file_DI07_ZERM.txt -b $final_option $upload_option $year $month $day
# fi

# ./compute_solution.sh -p config/parameter_file_DI02.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_DI03.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_DI04.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_DI05.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_DI07.txt -b $final_option $upload_option $year $month $day

# ./compute_solution.sh -p config/parameter_file_DI55.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_DI57.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_GU02.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_GU03.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_GU04.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_ST02.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_ST05.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_LS01.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_LS04.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_LS05.txt -b $final_option $upload_option $year $month $day

# ./compute_solution.sh -p config/parameter_file_LS11.txt -b $final_option $upload_option $year $month $day
./compute_solution_1h.sh -p config/parameter_file_LS11_fast.txt -b $final_option $upload_option $year $month $day
# # LS11 logger only 2020-02-26 to 2020-04-04
# LS11_stop_date=$(date -d "2020-02-26" '+%Y%m%d')
# LS11_restart_date=$(date -d "2020-04-04" '+%Y%m%d')
# solution_date=$(date -d "$year-$month-$day" '+%Y%m%d')
# if [[ $solution_date -gt $LS11_stop_date ]] && [[ $solution_date -lt $LS11_restart_date ]]; then
#   ./compute_solution.sh -p config/parameter_file_LS11.txt -b -r -c $final_option $upload_option $year $month $day
# fi
# ./compute_solution.sh -p config/parameter_file_LS12.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_LS06.txt -b $final_option $upload_option $year $month $day

# # # excute Grabengufer 2011-09-29 272
# ./compute_solution.sh -p config/parameter_file_RG01_ZERM.txt -b  $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_RG01_RAND.txt -b  $final_option $upload_option $year $month $day

./compute_solution_1h.sh -p config/parameter_file_GG01_fast.txt -b  $final_option $upload_option $year $month $day
./compute_solution_1h.sh -p config/parameter_file_GG02_fast.txt -b  $final_option $upload_option $year $month $day
# ##./compute_solution.sh -p config/parameter_file_GG51.txt -b  $year $month $day
# ./compute_solution.sh -p config/parameter_file_GG52.txt -b  $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_GG66.txt -b  $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_GG67.txt -b  $final_option $upload_option $year $month $day

# # excute Sattelspitz
#  ./compute_solution.sh -p config/parameter_file_SATT_RAND.txt -b  $final_option $upload_option $year $month $day
#  ./compute_solution.sh -p config/parameter_file_SA01_RAND.txt -b  $final_option $upload_option $year $month $day

# # execute PERMOS
# # start 2012 07 19 is 2012 201
# ./compute_solution.sh -p config/parameter_file_DIS1.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_DIS2.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_RIT1.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_GRU1.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_JAE1.txt -b $final_option $upload_option $year $month $day

# # switching from SAME to SAM2, start date for PNAC SAME2 GNSS station
# SAME_stop_date=$(date -d "2016-03-02" '+%Y%m%d')
# solution_date=$(date -d "$year-$month-$day" '+%Y%m%d')
# if [[ $solution_date -ge $SAME_stop_date ]]
# then
#   ./compute_solution.sh -p config/parameter_file_SCH1.txt -b $final_option $upload_option $year $month $day
#   ./compute_solution.sh -p config/parameter_file_MUA1.txt -b $final_option $upload_option $year $month $day
#   ./compute_solution.sh -p config/parameter_file_COR1.txt -b $final_option $upload_option $year $month $day
# else
#   ./compute_solution.sh -p config/parameter_file_SCH1_SAME.txt -b $final_option $upload_option $year $month $day
#   ./compute_solution.sh -p config/parameter_file_MUA1_SAME.txt -b $final_option $upload_option $year $month $day
#   ./compute_solution.sh -p config/parameter_file_COR1_SAME.txt -b $final_option $upload_option $year $month $day
# fi

# ./compute_solution.sh -p config/parameter_file_LAR1.txt -b $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_LAR2.txt -b $final_option $upload_option $year $month $day

#greenland
# ./compute_solution.sh -p config/parameter_file_greenland_EB06.txt -b -r  -c $year $month $day
# ./compute_solution.sh -p config/parameter_file_greenland_EB24.txt -b -r  -c $year $month $day
# ./compute_solution.sh -p config/parameter_file_greenland_EG09.txt -b -r  -c $year $month $day
# ./compute_solution.sh -p config/parameter_file_greenland_EG14.txt -b -r  -c $year $month $day
# ./compute_solution.sh -p config/parameter_file_greenland_EG19.txt -b -r  -c $year $month $day
# ./compute_solution.sh -p config/parameter_file_greenland_EG26.txt -b -r  -c $year $month $day

# zmuttgletscher
# ./compute_solution.sh -p config/parameter_file_ZM01_ZERM.txt -b -r $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_ZM02_ZERM.txt -b -r $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_ZM03_ZERM.txt -b -r $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_ZM04_ZERM.txt -b -r $final_option $upload_option $year $month $day
# ./compute_solution.sh -p config/parameter_file_ZM05_ZERM.txt -b -r $final_option $upload_option $year $month $day

# discard ubx files -> they are deleted together with the temp_dir
temp_dir="/tmp/temp_"${year}${month}${day}
# rm -r $temp_dir 
rm -r /tmp/slurm_gps_compute_$SLURM_JOB_ID.err
echo "Deletes the temporary folder $temp_dir and stderr temp file /tmp/slurm_gps_compute_$SLURM_JOB_ID.err."


echo finished at: `date`

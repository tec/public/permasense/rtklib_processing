'''
Collection of functions converting a conventional (year, month, day) date into different gps date formats
The functions gpsFromUTC, gpsWeek and dayOfWeek are copied from : https://github.com/skymoo/lalsuite/blob/master/glue/glue/gpstime.py
'''

import urllib3,datetime,sys,time,getopt,math
from time import strftime

secsInWeek = 604800
secsInDay = 86400
gpsEpoch = (1980, 1, 6, 0, 0, 0)  # (year, month, day, hh, mm, ss)

# function returning the day of the year for a certain date
def dayOfYear(year, month, day):
	hr=12
	t = time.mktime((year, month, day, hr, 0, 0, 0, 0, -1))
	pyDoy = time.localtime(t)[7]
	return pyDoy

# function returning the day of the year for a certain date
def dayOfWeek(year, month, day):
	"returns day of week: 0=Sun, 1=Mon, .., 6=Sat"
	hr = 12
	t = time.mktime((year, month, int(day), hr, 0, 0, 0, 0, -1))
	pyDow = time.localtime(t)[6]
	gpsDow = (pyDow + 1) % 7
	return gpsDow

# function returning the gps week for a certain date
def gpsWeek(year, month, day):
	"returns (full) gpsWeek for given date (in UTC)"
	hr = 12
	#print "called gpsWeek"
	return gpsFromUTC(year, month, day, hr, 0, 0.0)[0]

# function returning the gps day of the week for a certain date
def gpsDay(year, month, day):
	"returns (full) gps day of the week for given date (in UTC)"
	hr = 12
	return gpsFromUTC(year, month, day, hr, 0, 0.0)[0]

# function returning the gps week, seconds ow week, the gps day and the seconds of the day for a given date
def gpsFromUTC(year, month, day, hour, min, sec, leapSecs=14):
	secFract = sec % 1
	epochTuple = gpsEpoch + (-1, -1, 0)
	t0 = time.mktime(epochTuple)
	t = time.mktime((year, month, int(day), hour, min, int(sec), -1, -1, 0))
	t = t + leapSecs
	tdiff = t - t0
	gpsSOW = (tdiff % secsInWeek)  + secFract
	gpsWeek = int(math.floor(tdiff/secsInWeek))
	gpsDay = int(math.floor(gpsSOW/secsInDay))
	gpsSOD = (gpsSOW % secsInDay)
	return (gpsWeek, gpsSOW, gpsDay, gpsSOD)

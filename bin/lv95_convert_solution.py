'''
convert rtklib lvh positions to swiss grid lv95, bessel (ellipsoidal)

Usage: python lv95_convert_solution.py $file
       python lv95_convert_solution.py $file 17 DI07 RD01

'''

from datetime import datetime
from pytz import timezone
import os.path, time
import sys
import json
from decimal import Decimal
import urllib
from urllib.request import urlopen

posfile=sys.argv[1]
opened_file=open(posfile, "r")
parsing_position = False;

for parseline in opened_file:
    if parseline.startswith("% obs start"):
        one,two,three,four,GPSDate,GPSTime,seven,eight,nine=parseline.split()
        #print GPSDate
        #print GPSTime
        pass
    elif parseline.startswith("%"):
        pass
    else:
        parsing_position = True;
        GPSDate,GPSTime,northing,easting,altitude,Q,ns,sdx,sdy,sdz,sdxy,sdyz,sdzx,age,ratio=parseline.split()
opened_file.close()

if not parsing_position:
    print(GPSDate + " " + GPSTime + "00 NA NA NA NA NA NA NA NA NA NA NA NA NA")
    exit()
else:
    #res = json.load(urllib2.urlopen('http://geodesy.geo.admin.ch/reframe/wgs84tolv95?easting=7.670407897&northing=45.980922022&altitude=3537.1377&format=json'))
    myurl = urllib.parse.urlencode({'easting':easting,'northing':northing,'altitude':altitude,'format':'json'})
    # print(myurl)
    res = json.load(urlopen("http://geodesy.geo.admin.ch/reframe/wgs84tolv95?%s" % myurl), parse_int=Decimal, parse_float=Decimal)
    easting  = str(round(float(res['easting']), 4))
    northing = str(round(float(res['northing']), 4))
    altitude = str(round(float(res['altitude']), 4))
    # print(res['northing'])
    # print(res['altitude'])
    #print("%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s" %(GPSDate,GPSTime,res['easting'],res['northing'],res['altitude'],Q,ns,sdx,sdy,sdz,sdxy,sdyz,sdzx,age,ratio))
    if len(sys.argv)==2:
        print(GPSDate + " " + GPSTime + " " + easting + " " + northing + " " + altitude + " " + Q + " " + ns + " " + sdx + " " + sdy + " " + sdz + " " + sdxy + " " + sdyz + " " + sdzx + " " + age + " " + ratio)
        exit()
    else:
        if (sys.argv[5] == "3") :
            struct_time = time.strptime(GPSDate + " " + GPSTime + " UTC", "%Y/%m/%d %H:%M:%S.%f %Z")
        else:
            struct_time = time.strptime(GPSDate + " 13:00:00 UTC", "%Y/%m/%d %H:%M:%S %Z")
        #print struct_time

        #type,position,device_id,device_type,generation_time,processing_time,gps_week,gps_tow,x-ecef,y-ecef,z-ecef,sdx,sdy,sdz,sdxy,sdyz,sdzx,E,N,H,sdE,sdN,sdH,sd_EN,sd_EH,sd_NH,Q,ns,age,ratio,version,comments,label,reference_label
        #2,35,3500,wgps2,1355318714098,,,,,,,,,,,,628988.010908,104371.373255,2970.623781,1.31210566504,1.01081288957,2.09938499234,0.74131266812,-1.81633778669,-1.03099244637,2,6,-0.00,1.5,4,test,upload,TEST,TEST
        # 0 - bernese, 1 - rtklib rt, 2 - rtklib batch, 3 - rtklib6h
        print(sys.argv[5] + ","+sys.argv[2]+",,wireless-gps," + str(int(time.mktime(struct_time)*1000))+","+str(int(time.time()*1000))+",,,,,,,,,,,," + easting+","+northing+","+altitude+","+sdx+","+sdy+","+sdz+","+sdxy+","+sdyz+","+sdzx+","+Q+","+ns+","+age+","+ratio+",4, RTKLIB ver.2.4.3 @ ETH Zurich,"+sys.argv[3]+","+sys.argv[4]+"\n")
 
    
#!/bin/bash
#
# IGS Rapid download script
#
#/bin/echo Input: $1 $2    YEAR DoY


#echo $number_of_opts
i=$((number_of_opts+1))
year=$(eval echo \${$i})
# echo $year
i=$((i+1))
DoYarg=$(eval echo \${$i})
# echo $DoYarg

year=$(date -d "$year-01-01 +$DoYarg days -1 day" "+%Y")
month=$(date -d "$year-01-01 +$DoYarg days -1 day" "+%m")
day=$(date -d "$year-01-01 +$DoYarg days -1 day" "+%d")

# year="2019"
# month="01"
# day="01"

year_short=${year: -2}
sec_in_week="$((60*60*24*7))"
# unix_to_gps_offset=315964800
# unix_to_gps_offset=315961200
unix_to_gps_offset=315957600

doy=$(date -d ${year}-${month}-${day} +%j) 
dow=$(date -d ${year}-${month}-${day} +%w) 

gps_secs=$(date -d ${year}-${month}-${day} +%s) 

# echo "$unix_to_gps_offset"
if [ $year -ge 2017 ]; then
    leap_seconds="18"
    unix_to_gps_offset=$((unix_to_gps_offset-$leap_seconds))
    # echo "$unix_to_gps_offset"
elif [ $year -ge 2015 ]; then
    if [ month -ge 07 ]; then
        leap_seconds="17"
        unix_to_gps_offset=$((unix_to_gps_offset-$leap_seconds))
    fi
    # echo "$unix_to_gps_offset"
elif [ $year -ge 2012 ]; then
    if [ $month -ge 07 ]; then
        leap_seconds="16"
        unix_to_gps_offset=$((unix_to_gps_offset-$leap_seconds))
    fi
    # echo "$unix_to_gps_offset"
elif [ $year -ge 2009 ]; then
    leap_seconds="15"
    unix_to_gps_offset=$((unix_to_gps_offset-$leap_seconds))
    # echo "$unix_to_gps_offset"
elif [ $year -ge 2006 ]; then
    leap_seconds="14"
    unix_to_gps_offset=$((unix_to_gps_offset-$leap_seconds))
    # echo "$unix_to_gps_offset"
elif [ $year -ge 1999 ]; then
    leap_seconds="13"
    unix_to_gps_offset=$((unix_to_gps_offset-$leap_seconds))
    # echo "$unix_to_gps_offset"
fi

gps_corr_secs="$(($gps_secs-$unix_to_gps_offset))"
gps_week=$((gps_corr_secs / sec_in_week))

echo "Downloading IGS Daily Rapid data product $year $year_short $month $day $doy $gps_secs $gps_corr_secs $leap_seconds $gps_week $dow"
# echo "Downloading IGR data $year $year_short $month $day $doy $dow"

if (($(curl --silent -I https://cddis.nasa.gov/archive/gps/data/daily/${year}/${doy}/${year_short}n/brdc${doy}0.${year_short}n.Z | grep -E "^HTTP" | awk -F " " '{print $2}') == 200))
then 
    # echo "Zip"
    curl -f -s -R -c ~/.netrc_cookies -n -L -O "https://cddis.nasa.gov/archive/gps/data/daily/${year}/${doy}/${year_short}n/brdc${doy}0.${year_short}n.Z"
    if [ -f brdc${doy}0.${year_short}n.Z ]
    then
        gunzip -f brdc${doy}0.${year_short}n.Z
    fi
fi
if (($(curl --silent -I https://cddis.nasa.gov/archive/gps/data/daily/${year}/${doy}/${year_short}n/brdc${doy}0.${year_short}n.gz | grep -E "^HTTP" | awk -F " " '{print $2}') == 200))
then 
    # echo "gzip"
    curl -f -s -R -c ~/.netrc_cookies -n -L -O "https://cddis.nasa.gov/archive/gps/data/daily/${year}/${doy}/${year_short}n/brdc${doy}0.${year_short}n.gz"
    if [ -f brdc${doy}0.${year_short}n.gz ]
    then
        gunzip -f brdc${doy}0.${year_short}n.gz
    fi
fi

#move to igs_data repository
# ls -al brdc${doy}0.*
mv brdc${doy}0.${year_short}n ../igs_data
# rm brdc${doy}0.*

curl -f -s -R -c ~/.netrc_cookies -n -L -O "https://cddis.nasa.gov/archive/gps/products/ionex/${year}/${doy}/igrg${doy}0.${year_short}i.Z"
ls -al igrg${doy}0.${year_short}i.*
if [ -f igrg${doy}0.${year_short}i.Z ]
then
    gunzip -f igrg${doy}0.${year_short}i.Z
fi
ls -al igrg${doy}0.${year_short}i*

gps_secs=$(date -d ${year}-${month}-${day} +%s) 
gps_corr_secs="$(($gps_secs-$unix_to_gps_offset))"
gps_week=$((gps_corr_secs / sec_in_week))

echo "Now for week $year $year_short $sec_in_week $gps_secs $gps_corr_secs $gps_week $dow"

# for dow in {0..6}
#     do
    curl -f -s -R -c ~/.netrc_cookies -n -L -O "https://cddis.nasa.gov/archive/gps/products/${gps_week}/igr${gps_week}${dow}.clk.Z"
    if [ -f igr${gps_week}${dow}.clk.Z ]
    then
        gunzip -f igr${gps_week}${dow}.clk.Z
    fi
    # curl -s -R -c ~/.netrc_cookies -n -L -O "https://cddis.nasa.gov/archive/gps/products/${gps_week}/igr${gps_week}${dow}.clk_30s.Z"
    # gunzip -f igs${gps_week}${dow}.clk_30s.Z
    curl -f -s -R -c ~/.netrc_cookies -n -L -O "https://cddis.nasa.gov/archive/gps/products/${gps_week}/igr${gps_week}${dow}.sp3.Z"
    if [ -f igr${gps_week}${dow}.sp3.Z ]
    then
        gunzip -f igr${gps_week}${dow}.sp3.Z
    fi
    curl -f -s -R -c ~/.netrc_cookies -n -L -O "https://cddis.nasa.gov/archive/gps/products/${gps_week}/igr${gps_week}${dow}.erp.Z"
    if [ -f igr${gps_week}${dow}.erp.Z ]
    then
        gunzip -f igr${gps_week}${dow}.erp.Z
    fi
# done

# ls -al igr${gps_week}*

echo "Done. Ready for ice cream."
echo 


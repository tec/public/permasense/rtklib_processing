#!/bin/bash
#
#slurm_gps_batch_processing.sh script for running RTKLIB daily on SLURM using cron
#
# author: Jan Beutel
# (c) University of Innsbruck, 2021
#

gps_processing_dir="/ifi-NAS/nes/research/gps"
gps_bin_dir=${gps_processing_dir}"/rtklib_processing/bin"
external_dataproducts_dir=${gps_processing_dir}"/external_dataproducts"
gps_archive_dir=${gps_processing_dir}"/data_archive"
gps_rtk_dir=${gps_processing_dir}"/rtklib_processing"

# result_dir=${prefix_dir}"/rtklib_processing"
# temp_dir="/tmp/temp_$year$mmonth$dday"
slurm_bin_dir="/usr/local/slurm/bin"

delay=30

DoY=$(date +%-j)
year=$(date "+%y")
Year=$(date "+%Y")
now=$(date)

# printf -v DDoY '%03d' $DoY
# printf -v dday "%02d" ${day#0}
echo "#########################################################################"
echo "SLURM GPS Batch Processing"
echo "SLURM Starting on: $now"
echo $Year $year $DoY

# echo "Synchronizing PNAC data delivery from ETHZ"
rsync -travz jbeutel@whymper.ethz.ch:/home/jbeutel/permasense_vault/data_archive/gps_data/swisstopo_auto_delivery ${gps_archive_dir}
du -sm ${gps_archive_dir}/swisstopo_auto_delivery
# rsync -travz jbeutel@tik43x.ethz.ch:/home/jbeutel/permasense_vault/gps/rtklib_processing/pnac ${gps_processing_dir}/rtklib_processing
# du -sm ${gps_processing_dir}/rtklib_processing/pnac/*

# Downloading new RINEX data from GGL
echo "Downloading new RINEX data from GGL."
# cd ${gps_bin_dir}
# ./get_ggl_auto_download.sh
cd ${gps_archive_dir}/ggl_auto_download
# lftp -e "mirror  --parallel=3 --verbose .misc/tijm9043vn043/tik/COGEAR ${gps_archive_dir}/ggl_auto_download; quit" http://hera.ethz.ch
wget -q --no-parent -r -nH --cut-dirs=4 -nv http://hera.ethz.ch/.misc/tijm9043vn043/tik/COGEAR
ls ${gps_archive_dir}/ggl_auto_download
echo "All rinex data downloaded from GGL."

# Local GPS Archive Update
cd ${gps_bin_dir}
${gps_bin_dir}/gps_archive_update.sh -2
${gps_bin_dir}/gps_archive_update.sh

#
# Compute Final positions
#
for final_offset in 41 40 39 38 37 36 35 34 33 32; do
# for final_offset in 28 27 26; do
  # echo ""
  echo "###################"
  runDoY="$((DoY - $final_offset))"
  year=$(date -d "$Year-01-01 +$runDoY days -1 day" "+%Y")
  month=$(date -d "$Year-01-01 +$runDoY days -1 day" "+%m")
  day=$(date -d "$Year-01-01 +$runDoY days -1 day" "+%d")
  DDoY=$(date -d "$Year-01-01 +$runDoY days -1 day" "+%j")
  # year=$(date -d "$Year-01-01 +$runDoY days" "+%Y")
  # month=$(date -d "$Year-01-01 +$runDoY days" "+%m")
  # day=$(date -d "$Year-01-01 +$runDoY days" "+%d")
  # DDoY=$(date -d "$Year-01-01 +$runDoY days" "+%j")

  cd ${gps_bin_dir}
  echo "Computing RTKLIB Final Solution for $year $month $day, Year $year, single DoY $DDoY, offset $final_offset, $runDoY days with data download"
  # echo ""
  # echo "sbatch ./slurm_gps_compute.sh -d -u -f $year $DoY"
  ${slurm_bin_dir}/sbatch ./slurm_gps_compute.sh -d -u -f $year $DDoY
  # ./slurm_gps_compute.sh -d -u -f $year $DDoY
  sleep $delay
done

#
# Compute Rapid positions
#
for rapid_offset in 31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1; do
  # echo ""
  echo "###################"
  runDoY="$((DoY - $rapid_offset))"
  year=$(date -d "$Year-01-01 +$runDoY days -1 day" "+%Y")
  month=$(date -d "$Year-01-01 +$runDoY days -1 day" "+%m")
  day=$(date -d "$Year-01-01 +$runDoY days -1 day" "+%d")
  DDoY=$(date -d "$Year-01-01 +$runDoY days -1 day" "+%j")

  cd ${gps_bin_dir}
  echo "Computing RTKLIB Rapid Solution for $year $month $day, Year $year, single DoY $DDoY, offset $rapid_offset, $runDoY days with data download"
  #echo ""
  ${slurm_bin_dir}/sbatch ./slurm_gps_compute.sh -d -u $year $DDoY
  # ./slurm_gps_compute.sh -d -u $year $DDoY
  sleep $delay
done

echo ""
echo "Done."
echo ""

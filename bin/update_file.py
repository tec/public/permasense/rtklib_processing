'''
Script to find and replace an entire line in a text file. Used in order to update the paths in the parameter_file.txt
Usage: python update_file.py [arg1] [arg2] [arg3] [arg4]
	   arg1: the path to the file in which something shall be updated
	   arg2: the name of the file in which something shall be updated
	   arg3: the key whose value shall be updated
	   arg4: the new value for the key given in arg3

'''

import fileinput
import sys
import string
from tempfile import mkstemp
import os
import shutil

# creates a double of file and then replaces file by file with the new value for the given key
def replace(path,file,key,new_value):
	#os.mknod(path+"/"+file+"_initialvalues.txt")
	tempfilename=os.path.splitext(os.path.basename(file))[0]
	tempfilepath=path+"/"+tempfilename+"_not_updated.conf"
	shutil.copyfile(path+"/"+file, tempfilepath)
	oldfile=open(tempfilepath, "r")
	newfile=open(path+"/"+file, "w")
	for line in oldfile:
		if key in line:
			#print "found the key "+str(key)
			rest = line.split('=', 1)[0]
			newfile.write(rest+"="+new_value+'\n')
		else:
			newfile.write(line)
	oldfile.close()
	newfile.close()

# this script's main function
def main():
	if len(sys.argv)!=5:
		message= "Fail: The script update_file.py was called with wrong amount of arguments. For correct usage please read the script's first lines."
		print(message)
	else:
		path=sys.argv[1]
		file=sys.argv[2]
		key=sys.argv[3]
		new_value=sys.argv[4]
		#message= "Update config-file: the key "+str(key)+" is assigned the new value "+str(new_value)+'\n'
		replace(path,file,key,new_value )
		#message=message+"The update was successful.\n"

if __name__ == "__main__":
   main()

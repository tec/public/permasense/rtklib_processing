#! /usr/bin/env python

'''
Script downloading a ubx file (GPS raw data) for a given position from the data.permasense.ethz.ch site

Usage: python download_gps_data.py -t <target> -p <pos> -l <label> -d <date> -n <deployment> -s <server>

<target> :     The directory into which the data is downloaded
<pos> :        The position-number of the station for which the data is required
<label> :      The label/name of the station for which the data is required
<date>:        The date for which the data is required
<deployment>:  The deployment in which the receiver is located
<server>:      The GSN server
'''

from urllib.request import urlopen
import datetime,calendar,sys,time,getopt,shutil,os
import GetUbxFileName

#This script's main function
def main(argv):
   log_message=''
   inputposition = ''
   label=''
   input_start_date = ''
   target=''
   deployment=''
   try:
      opts, args = getopt.getopt(argv,"hp:t:l:d:n:s:",["inputposition=", "target=" ,"label=" ,"input_start_date=", "deployment="])
   except getopt.GetoptError:
      log_message='The download_gps_data.py script was called wrongly. Correct usage: gps_raw_download.py -t <target> -p <position> -l <label> -d <start_date> -n <deployment> -s <server>'
      print(log_message)
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         log_message='The download_gps_data.py script was called wrongly. Correct usage: gps_raw_download.py -t <target> -p <position> -l <label> -d <start_date> -n <deployment> -s <server>'
         print(log_message)
         sys.exit()
      elif opt in ("-p", "--position"):
         inputposition = arg
      elif opt in ("-t", "--target"):
         target = arg
      elif opt in ("-l" , "--label"):
         label = arg
      elif opt in ("-d", "--date"):
         input_start_date = arg
      elif opt in ("-n", "--deployment"):
         deployment = arg
      elif opt in ("-s", "--server"):
         server = arg

   position = int(inputposition)
   start_timestamp = (int(calendar.timegm(time.strptime(input_start_date, "%d/%m/%Y")))) * 1000
   stop_timestamp  = (int(calendar.timegm(time.strptime(input_start_date, "%d/%m/%Y"))) + (60*60*24) - 1) * 1000


   year=(datetime.datetime.fromtimestamp(start_timestamp/1000).strftime('%Y'))
   month=(datetime.datetime.fromtimestamp(start_timestamp/1000).strftime('%m'))
   day=(datetime.datetime.fromtimestamp(start_timestamp/1000).strftime('%d'))

   print('Downloading GPS data for Position ',position)
   print('From',datetime.datetime.utcfromtimestamp(start_timestamp/1000).strftime('%d/%m/%Y %H:%M:%S'),'unixtime',start_timestamp/1000,'to',datetime.datetime.utcfromtimestamp(stop_timestamp/1000).strftime('%d/%m/%Y %H:%M:%S'),'unixtime',stop_timestamp/1000,'UTC')
   #url = "http://data.permasense.ch/multidata?vs[1]=dirruhorn_gps_raw__binary__mapped&field[1]=gps_raw_data&download_format=binary&order=asc&c_join[1]=and&c_vs[1]=dirruhorn_gps_raw__binary__mapped&c_field[1]=position&c_min[1]="+str(position-1)+"&c_max[1]="+str(position)+"&c_vs[2]=dirruhorn_gps_raw__binary__mapped&c_join[2]=and&c_field[2]=gps_missing_sv&c_min[2]=-inf&c_max[2]=0&c_vs[3]=dirruhorn_gps_raw__binary__mapped&c_join[3]=and&c_field[3]=gps_unixtime&c_min[3]="+str(start_timestamp)+"&c_max[3]="+str(stop_timestamp)+"&timeline=gps_unixtime&time_format=unix HTTP/1.1"
   #url = "http://data.permasense.ch/multidata?vs[1]="+str(deployment)+"_gps_raw__binary__mapped&field[1]=gps_raw_data&download_format=binary&order=asc&c_join[1]=and&c_vs[1]="+str(deployment)+"_gps_raw__binary__mapped&c_field[1]=position&c_min[1]="+str(position-1)+"&c_max[1]="+str(position)+"&c_vs[2]="+str(deployment)+"_gps_raw__binary__mapped&c_join[2]=and&c_field[2]=gps_missing_sv&c_min[2]=-inf&c_max[2]=0&c_vs[3]="+str(deployment)+"_gps_raw__binary__mapped&c_join[3]=and&c_field[3]=gps_unixtime&c_min[3]="+str(start_timestamp)+"&c_max[3]="+str(stop_timestamp)+"&timeline=gps_unixtime&time_format=unix"
   url = "http://"+str(server)+"/multidata?vs[1]="+str(deployment)+"_gps_raw__binary__mapped&field[1]=gps_raw_data&download_format=binary&order=asc&c_join[1]=and&c_vs[1]="+str(deployment)+"_gps_raw__binary__mapped&c_field[1]=position&c_min[1]="+str(position-1)+"&c_max[1]="+str(position)+"&c_vs[2]="+str(deployment)+"_gps_raw__binary__mapped&c_join[2]=and&c_field[2]=gps_missing_sv&c_min[2]=-inf&c_max[2]=0&c_vs[3]="+str(deployment)+"_gps_raw__binary__mapped&c_join[3]=and&c_field[3]=gps_unixtime&c_min[3]="+str(start_timestamp)+"&c_max[3]="+str(stop_timestamp)+"&timeline=gps_unixtime&time_format=unix"
   print('URL called:',url)

   #file_name = inputposition+"_"+str(datetime.datetime.fromtimestamp(start_timestamp/1000).strftime('%Y%m%d'))+".ubx"
   #file_name=GetObsFileName.get_obs_fileName(label, year, month, day)

   file_name=GetUbxFileName.get_ubx_fileName(label,year,month,day)
   f = open(target+"/"+file_name, 'wb')
   response = urlopen(url)
   data = response.read()
   f.write(data)
   f.close()

   file_size = str(os.path.getsize(target+"/"+file_name))
   log_message=log_message+'\n'+"The downloaded data have been written into: "+target+"/"+file_name+" File size: "+file_size+'\n'
   #log_message=log_message+"Downloaded the ubx file to "+target+"/"+file_name

   print(log_message)

if __name__ == "__main__":
   main(sys.argv[1:])

# Scripts for GNSS processing using RTKLIB

This is a set of scripts to automate the computation of double-differential static GNSS solutions using rtklib. The scripts are configurable and can be run on a cluster using SLURM to speed things up. Configuration files and paths are of course set up for specific processing of PermaSense GNSS data, therefore any user needs to adapt these accordingly. Furthermore some double-differential baselines require specific observation data, e.g. from SwissTopo.

Author: Jan Beutel
(c) ETH Zurich/University of Innsbruck, 2021

## What you need

* RTKLIB http://www.rtklib.com/
* or alternatively https://github.com/rtklibexplorer/RTKLIB
* RNX2CRX/CRX2RNX - RINEX file compression programs for RINEX version 2/3 files. https://terras.gsi.go.jp/ja/crx2rnx.html

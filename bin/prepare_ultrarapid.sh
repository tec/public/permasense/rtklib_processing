#!/bin/bash
#
# Script calculating a gps sensor's position for a certain day using rtklib.
#
# creates a temporary folder temp_yyyymmdd at .. relative to this scrips execution path.
# writes a log file log_yyyymmdd.txt in the temporary folder
#
# all other directories involved must be declared in a separate parameter file
# this parameter file's path must be given as command line argument
#
# Usage: bash compute_solution.sh -p [parameter-file] [-d] [-b] [-qr] [-c] [-f] [-u] YYYY MM DD
#
# options:
#	  -d: igs data download
#	  -b: no data download and no conversion for the basestation
#	  -r: no data download and no conversion for the roverstation
#     -c: no conversion
#     -f: use IGS final data product
#     -u: upload to GSN database
#
# author: Samuel Zumtaugwald, Jan Beutel
# (c) ETH Zurich, 2018


#################################################################################################################################################################
# Get input arguments
#################################################################################################################################################################

if [[ $# -lt 3 ]] ; then
    echo "Invalid arguments. Usage: compute_solution.sh -p [parameter_file] -d -b -r -c -f -u YYYY MM DD"
    echo ""
    exit 1
fi

opt_dl=false  	  #bool indicating if the option -d is set
opt_bs=false		  #bool indicating if the option -b is set
opt_rs=false		  #bool indicating if the option -r is set
opt_cv=false		  #bool indicating if the option -c is set
opt_fi=false		  #bool indicating if the option -f is set
opt_up=false		  #bool indicating if the option -u is set
opt_pf=false 		  #bool indicating if the option -p was set: this one is mandatory !!
number_of_opts=0	#count of chosen options

while getopts "p:dbrcfu" options; do
  case $options in
    d ) opt_dl=true
		number_of_opts=$((number_of_opts+1))
	;;
    b ) opt_bs=true
		number_of_opts=$((number_of_opts+1))
	;;
    r ) opt_rs=true
		number_of_opts=$((number_of_opts+1))
	;;
    c ) opt_cv=true
		number_of_opts=$((number_of_opts+1))
	;;
    f ) opt_fi=true
    number_of_opts=$((number_of_opts+1))
  	;;
    u ) opt_up=true
    number_of_opts=$((number_of_opts+1))
  	;;
    p ) opt_pf=true
    parameter_file=$OPTARG
    number_of_opts=$((number_of_opts+2))
  	;;
	\?) echo "Invalid arguments. Usage: compute_solution.sh -p [parameter_file] -d -b -r -c -f -u YYYY MM DD"
    	echo ""
      exit 1
  esac
done

if [ $opt_pf == false ]
then
	echo "A path to a parameterfile must be given. Usage: compute_solution.sh -p [parameter_file] -d -b -r -c -f -u YYYY MM DD"
	echo ""
  exit 1
fi

if ! [ -f "$parameter_file" ]
then
	echo "The parameter_file was not found at $parameter_file"
	echo "Please verify its path in the command line. Usage: compute_solution.sh -p [parameter_file] -d -b -r -c -f -u YYYY MM DD"
	echo ""
  exit 1
fi

i=$((number_of_opts+1))
year=$(eval echo \${$i})
i=$((i+1))
month=$(eval echo \${$i})
month=$((10#$month))
i=$((i+1))
day=$(eval echo \${$i})
day=$((10#$day))

if [ "$year" -lt 1980 ]
then
	echo "A year before 1980 is not valid!"
	echo ""
  exit 1
elif [ "$month" -lt 1 ] || [ "$month" -gt 12 ]
then
	echo "The month must be between 1 and 12!"
	echo ""
  exit 1
elif [ "$day" -lt 1 ] || [ "$day" -gt 31 ]
then
	echo "The day must be between 1 and 31!"
	echo ""
  exit 1
fi

#printf "Year Month Day: %d %02d %02d\n" $year $month $day
printf -v month2 "%02d" $month 		#month2 is the 2-digit version of month
printf -v day2 "%02d" $day			#day2 is the 2-digit version of day

#The directory this script is stored in
basedir="$(dirname $(readlink -f $0))"

# Start
datetime_start=$(date '+%d/%m/%Y %H:%M:%S')
echo "Started at: $datetime_start"
echo "Called the compute_solution.sh script for date: $year $month2 $day2, $parameter_file"

# Create temporary directory
#temp_dir="$basedir"/../temp_$year$month2$day2
temp_dir="/tmp/temp_"$year$month2$day2

if [ -d "$temp_dir" ]
then
	rm "-r" $temp_dir
	mkdir "$temp_dir"
else
	mkdir "$temp_dir"
fi

if ! [ -d $temp_dir ]
then
	echo "Failed to create the temp folder"
	echo ""
  exit 1
else
	echo "Created temporary folder $temp_dir"
fi

# echo the shosen options
#echo "opt_bs: "$opt_bs
#echo "opt_rs: "$opt_rs
#echo "opt_dl: "$opt_dl
#echo "opt_cv: "$opt_cv
if $opt_bs
then
	echo "The -b option was set. Basestation observations are not downloaded."
fi
if $opt_rs
then
	echo "The -r option was set. Roverstation observations are not downloaded."
fi
if $opt_cv
then
	echo "The -c option was set. No RINEX conversion."
fi
if $opt_fi
then
	echo "The -f option was set. Using IGS final data."
fi
if $opt_dl
then
	echo "The -d option was set. IGS files are downloaded."
fi
if $opt_up
then
	echo "The -u option was set. Uploading file to GSN database server."
fi

#################################################################################################################################################################
# Get the parameters from the parameter_file
#################################################################################################################################################################
echo "Loading configuration parameters ..."
cp "$parameter_file" "$temp_dir/parameter_file.txt"

#####_____________________________DIRECTORIES_____________________________#####
# Parse the 'directories' section of the parameter_file
# In the used configuration/parameterfile, the paths have to be absolute
#echo "Loading the file paths from the [directories] section of the configuration file"

# read the paths from the parameter file
igs_data_dir=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "directories" "igs_data_dir")
igs_data_dir='/ifi-NAS/nes/research/gps/external_dataproducts/igu_data'
igr_data_dir=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "directories" "igr_data_dir")
code_data_dir=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "directories" "code_data_dir")
rtklib_dir=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "directories" "rtklib_dir")
rtklib_options_dir=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "directories" "rtklib_options_dir")
gps_data_dir=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "directories" "gps_data_dir" )
output_dir=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "directories" "output_dir")

#echo "Check the existence of the directories: "
# bool indicating if any directories given in the parameter_file weren't found
missing_directories=false
if ! [ -d "$igs_data_dir" ]
then
	echo "Missing directory igs_data_dir:  $igs_data_dir "
	missing_directories=true
fi
if ! [ -d "$igr_data_dir" ]
then
	echo "Missing directory igr_data_dir:  $igr_data_dir "
	missing_directories=true
fi
if ! [ -d "$code_data_dir" ]
then
	echo "Missing directory code_data_dir:  $code_data_dir "
	missing_directories=true
fi
if ! [ -d "$rtklib_dir" ]
then
	echo "Missing directory rtklib_dir:  $rtklib_dir  "
	missing_directories=true
fi
if ! [ -d "$rtklib_options_dir" ]
then
	echo "Missing directory rtklib_options_dir:  $rtklib_options_dir  "
    	missing_directories=true
fi
if ! [ -d "$gps_data_dir" ]
then
	echo "Missing directory gps_data_dir :  $gps_data_dir  "
	missing_directories=true
fi
if ! [ -d "$output_dir" ]
then
	echo "Missing directory output_dir:  $output_dir  "
	missing_directories=true
fi

#if any of the directories doesn't exit, stop this script
if  $missing_directories
then
	#echo Missing_directories: $missing_directories
  echo "The direcories listed couldn't be found."
	echo ""
  exit 1
#else
#	echo "All directories were found."
fi

# bool indicating if any key was found in the parameter file
parsing_errors_directories=false

# check if in any of the ConfigReader.py calls the parameter file was not found
if [ "$igs_data_dir" == "InvConf" ] || [ "$igr_data_dir" == "InvConf" ] || [ "$code_data_dir" == "InvConf" ] || [ "$rtklib_dir" == "InvConf" ] || [ "$rtklib_options_dir" == "InvConf" ] || [ "$gps_data_dir" == "InvConf" ] || [ "$output_dir" == "InvConf" ]
then
	echo "The parameter_file was not found at $parameter_file"
	parsing_errors_directories=true
fi
# check if in any of the ConfigReader.py calls the section [directories] was not found
if [ "$igs_data_dir" == "InvSec" ] || [ "$igr_data_dir" == "InvSec" ] || [ "$code_data_dir" == "InvSec" ] || [ "$rtklib_dir" == "InvSec" ] || [ "$rtklib_options_dir" == "InvSec" ] || [ "$gps_data_dir" == "InvSec" ] || [ "$output_dir" == "InvSec" ]
then
	echo "Error while parsing $parameter_file: The section [directories] was not found"
	parsing_errors_directories=true
fi
# check if in any of the ConfigReader.py calls the key was not found
if [ "$igs_data_dir" == "InvOpt" ]
then
	echo "Error while parsing $temp_dir/parameter_file.txt The key 'igs_data_dir' was not found"
	parsing_errors_directories=true
fi
if [ "$igr_data_dir" == "InvOpt" ]
then
	echo "Error while parsing $temp_dir/parameter_file.txt The key 'igr_data_dir' was not found"
	parsing_errors_directories=true
fi
if [ "$code_data_dir" == "InvOpt" ]
then
	echo "Error while parsing $temp_dir/parameter_file.txt The key 'codedata_dir' was not found"
	parsing_errors_directories=true
fi
if [ "$rtklib_dir" == "InvOpt" ]
then
	echo "Error while parsing $temp_dir/parameter_file.txt: The key 'rtklib_dir' was not found"
	parsing_errors_directories=true
fi
if [ "$rtklib_options_dir" == "InvOpt" ]
then
  echo "Error while parsing $temp_dir/parameter_file.txt: The key 'rtklib_options_dir' was not found"
	parsing_errors_directories=true
fi
if [ "$gps_data_dir" == "InvOpt" ]
then
	echo "Error while parsing $temp_dir/parameter_file.txt The key 'gps_data_dir' was not found"
	parsing_errors_directories=true
fi
if [ "$output_dir" == "InvOpt" ]
then
	echo "Error while parsing $temp_dir/parameter_file.txt The key 'output_dir' was not found"
	parsing_errors_directories=true
fi

# if any errors occured while parsig the [directories] then stop this script
if $parsing_errors_directories
then
  echo "Please verify the configuration parameter file and location."
	echo ""
  exit 1
#else
#	echo "All required keys in the [directories] section of the configuration file were found."
fi


#####________________________________FILES________________________________#####
# Get the 'files' section of the parameter_file
# Get further needed files such as th configuration file for the rtklib calculations (rnx2rtkp)
#echo "Loading the [files] section of configuration file."

#boolean indicating if any key in the [files] section wasn't found
parsing_errors_files=false
rnx2rtkp_config_file=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "files" "rtklib_conf_file")

if [ "$rnx2rtkp_config_file" == "InvSec" ]
then
	echo "Error while parsing the $parameter_file: The section [files] was not found."
	parsing_errors_files=true
fi
if [ "$rnx2rtkp_config_file" == "InvOpt" ]
then
	echo "Error while parsing the $parameter_file: The key 'rtklib_conf_file' was not found."
	parsing_errors_files=true
fi

# if any errors occured while parsig the [files] then stop this script
if $parsing_errors_files
then
	#echo "Parsing_errors_files: " $parsing_errors_files
	echo "Error while parsing the [files] section from the parameter file."
	echo ""
  exit 1
#else
#	echo "All required keys in the [files] section were found."
fi

#####______________________________POSITIONS______________________________#####
# Get the 'positions' section of the parameter_file
# Get the required information about the gps site to be calculated
#echo "Read the [positions] section of configuration file"

# bool indicating if any key in the [positions] section wasn't found
parsing_errors_positions=false
#echo "Retrieving the positioning data from the [positions] section within the configuration file"roverstation_deployment

# read the values from the config file 'parameter_file'
rover_station_deployment=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "positions" "rover_station_deployment")
rover_gsn_server=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "positions" "rover_gsn_server")
rover_station_nr=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "positions" "rover_station_nr")
rover_station_label=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "positions" "rover_station_label")
rover_start_date=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "positions" "rover_start_date")
rover_gsn_upload_server=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "positions" "rover_gsn_upload_server")
rover_gsn_upload_port=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "positions" "rover_gsn_upload_port")

base_station_deployment=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "positions" "base_station_deployment")
base_station_nr=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "positions" "base_station_nr")
base_station_label=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "positions" "base_station_label")
base_gsn_server=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "positions" "base_gsn_server")
ref_pos_x=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "positions" "ref_pos_x")
ref_pos_y=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "positions" "ref_pos_y")
ref_pos_z=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "positions" "ref_pos_z")

# create the used filenames
rover_station_gps_file=$(python -B GetUbxFileName.py $rover_station_label "$year" "$month" "$day")
base_station_gps_file=$(python -B GetUbxFileName.py $base_station_label "$year" "$month" "$day")
rover_station_obs_file=$(python -B GetObsFileName.py $rover_station_label "$year" "$month" "$day")
base_station_obs_file=$(python -B GetObsFileName.py $base_station_label "$year" "$month" "$day")

# check if in any of the ConfigReader.py calls the section [positions] was not found
if [ "$rover_station_nr" == "InvSec" ] || [ "$ref_pos_x" == "InvSec" ] || [ "$ref_pos_y" == "InvSec" ] || [ "$ref_pos_z" == "InvSec" ]
then
	echo "Error while parsing the $parameter_file: The section [positions] was not found."
	parsing_errors_positions=true
fi

# check if in any of the ConfigReader.py calls the key was not found
if [ "$rover_station_deployment" == "InvOpt" ]
then
	echo "Error while parsing the $parameter_file: The key 'rover_station_deployment' was not found."
	parsing_errors_positions=true
fi
if [ "$rover_gsn_server" == "InvOpt" ]
then
	echo "Error while parsing the $parameter_file: The key 'rover_gsn_server' was not found."
	parsing_errors_positions=true
fi
if [ "$rover_station_nr" == "InvOpt" ]
then
	echo "Error while parsing the $parameter_file: The key 'rover_station_nr' was not found."
	parsing_errors_positions=true
fi
if [ "$rover_station_label" == "InvOpt" ]
then
	echo "Error while parsing the $parameter_file: The key 'rover_station_label' was not found."
	parsing_errors_positions=true
fi
if [ "$rover_start_date" == "InvOpt" ]
then
	echo "Error while parsing the $parameter_file: The key 'rover_start_date' was not found."
	parsing_errors_positions=true
fi
if [ "$rover_gsn_upload_server" == "InvOpt" ]
then
	echo "Error while parsing the $parameter_file: The key 'rover_gsn_upload_server' was not found."
	parsing_errors_positions=true
fi
if [ "$rover_gsn_upload_port" == "InvOpt" ]
then
	echo "Error while parsing the $parameter_file: The key 'rover_gsn_upload_port' was not found."
	parsing_errors_positions=true
fi
if [ "$base_station_deployment" == "InvOpt" ]
then
	echo "Error while parsing the $parameter_file: The key 'base_station_deployment' was not found."
	parsing_errors_positions=true
fi
if [ "$base_gsn_server" == "InvOpt" ]
then
	echo "Error while parsing the $parameter_file: The key 'base_gsn_server' was not found."
	parsing_errors_positions=true
fi
if [ "$base_station_nr" == "InvOpt" ]
then
	echo "Error while parsing the $parameter_file: The key 'base_station_nr' was not found"
	parsing_errors_positions=true
fi
if [ "$base_station_label" == "InvOpt" ]
then
	echo "Error while parsing the $parameter_file: The key 'base_station_label' was not found."
	parsing_errors_positions=true
fi
if [ "$ref_pos_x" == "InvOpt" ]
then
	echo "Error while parsing the $parameter_file: The key 'ref_pos_x' was not found."
	parsing_errors_positions=true
fi
if [ "$ref_pos_y" == "InvOpt" ]
then
	echo "Error while parsing the $parameter_file: The key 'ref_pos_y' was not found."
	parsing_errors_positions=true
fi
if [ "$ref_pos_z" == "InvOpt" ]
then
	echo "Error while parsing the $parameter_file: The key 'ref_pos_z' was not found."
	parsing_errors_positions=true
fi

# if any errors occured while parsig the [positions] then stop this script
if $parsing_errors_directories
then
	echo "Error while parsing the [positions] section from the parameter file."
  echo ""
  exit 1
#else
#	echo "All required keys in the [positions] section were found."
fi

rover_start_date2=$(date -d "$(echo "$rover_start_date" | sed -r 's/(..).(..).(....)/\3-\2-\1/') -1 day" '+%Y%m%d')
#echo "$rover_start_date2"
solution_date=$(date -d "$year-$month2-$day2" '+%Y%m%d')
#echo "$solution_date"

if [ $rover_start_date2 -ge $solution_date ]
then
  echo "The Rover start date is only: $rover_start_date Abort."
  echo ""
  exit 1
fi

# now [positions] and [directories] are read. Create the entire output-directory and the gps directories
gps_data_dir_basestation="$gps_data_dir/$base_station_deployment/$base_station_label"
gps_data_dir_roverstation="$gps_data_dir/$rover_station_deployment/$rover_station_label"
output_dir_roverstation="$output_dir/$rover_station_deployment/$rover_station_label/results"

if ! [ -d "$gps_data_dir_basestation" ]
then
	echo "The directory $gps_data_dir_basestation doens't exist."
	echo ""
  exit 1
fi
if ! [ -d "$gps_data_dir_roverstation" ]
then
	echo "The directory $gps_data_dir_roverstation doens't exist."
	echo ""
  exit 1
fi
if ! [ -d "$output_dir_roverstation" ]
then
	echo "The directory $output_dir_roverstation doens't exist."
	echo ""
  exit 1
fi

#####_______________________________SERVERS_______________________________#####
# Get the required information about the servers from which to download required data
if  [ $opt_dl == true ]
then
	echo "Read the [servers] section of configuration file."
  #boolean indicating if any key in the [servers] section wasn't found
	parsing_errors_servers=false
  # sp3, clk and erp files
	dataserver_1=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "servers" "dataserver_1")
  # nav files
  dataserver_2=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "servers" "dataserver_2")
  # dcb files
	dataserver_3=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "servers" "dataserver_3")
  # iono files
  dataserver_4=$(python ConfigReader.py "$temp_dir/parameter_file.txt" "servers" "dataserver_4")

	if [ "$dataserver_1" == "InvSec" ] || [ "$dataserver_2" == "InvSec" ] || [ "$dataserver_3" == "InvSec" ] || [ "$dataserver_4" == "InvSec" ]
	then
		echo "Error while parsing the $parameter_file: The section [servers] was not found."
		parsing_errors_positions=true
	fi
	if [ "$dataserver_1" == "InvOpt" ]
	then
		echo "Error while parsing the $parameter_file: The key 'dataserver_1' was not found."
		parsing_errors_servers=true
	fi
	if [ "$dataserver_2" == "InvOpt" ]
	then
		echo "Error while parsing the $parameter_file: The key 'dataserver_2' was not found."
		parsing_errors_servers=true
	fi
	if [ "$dataserver_3" == "InvOpt" ]
	then
		echo "Error while parsing the $parameter_file: The key 'dataserver_3' was not found."
		parsing_errors_servers=true
	fi
  if [ "$dataserver_4" == "InvOpt" ]
	then
		echo "Error while parsing the $parameter_file: The key 'dataserver_4' was not found."
		parsing_errors_servers=true
	fi

	#if any errors occured while parsig the [positions] then stop this script
	if $parsing_errors_servers
	then
		#echo "parsing_errors_servers: " $parsing_errors_servers
    echo "Error while parsing the [servers] section of the parameter file."
		echo ""
    exit 1
	else
		echo "All required keys in the [servers] section were found."
	fi
fi

# select final or rapid IGS data products

# IGS file selection
nav_file=$(python -B -c 'import get_file_name; print(get_file_name.get_navu_fileName( '$year', '$month', '$day'))')

if $opt_fi
then
  #IGS Final
  echo "*** FINAL IGS data products selected."
  sp3_file=$(python -B -c 'import get_file_name; print(get_file_name.get_sp3_fileName( '$year', '$month', '$day'))')
  clk_file=$(python -B -c 'import get_file_name; print(get_file_name.get_clk_fileName( '$year', '$month', '$day'))')
  erp_file=$(python -B -c 'import get_file_name; print(get_file_name.get_erp_fileName( '$year', '$month', '$day'))')
  iono_file=$(python -B -c 'import get_file_name; print(get_file_name.get_iono_fileName( '$year', '$month', '$day'))')
  dcb_file=$(python -B -c 'import get_file_name; print(get_file_name.get_dcb_fileName( '$year', '$month'))')
  igr_data_dir=$igs_data_dir
  #echo "Using $igr_data_dir"
else
  #IGS Rapid
  echo "*** RAPID IGS data products selected."
  sp3_file=$(python -B -c 'import get_file_name; print(get_file_name.get_sp3_rapid_fileName( '$year', '$month', '$day'))')
  clk_file=$(python -B -c 'import get_file_name; print(get_file_name.get_clk_rapid_fileName( '$year', '$month', '$day'))')
  iono_file=$(python -B -c 'import get_file_name; print(get_file_name.get_iono_rapid_fileName( '$year', '$month', '$day'))')
  erp_file=$(python -B -c 'import get_file_name; print(get_file_name.get_erp_rapid_fileName( '$year', '$month', '$day'))')
fi


######
day_of_year=$(date -d $year/$month2/$day2 '+%j')
echo "Processing rover $rover_station_label position $rover_station_nr against base $base_station_label position $base_station_nr for $year $month2 $day2 day of the year $day_of_year"

#####______________________________option -r ______________________________#####
# if the -r option is set (i.e. the observation data for the rover station are already present), then check if all required gps files exist
if [ $opt_rs == true ]
then
	if ! [ -f "$gps_data_dir_roverstation/$rover_station_obs_file" ]
	then
		echo "The observation data file for the roverstation  $rover_station_label for $year $month2 $day2 was not found at $gps_data_dir_roverstation/$rover_station_obs_file."
		echo ""
    exit 1
	else
		cp "$gps_data_dir_roverstation/$rover_station_obs_file" "$temp_dir/$rover_station_obs_file"
		#echo "The roverstation's obs data $rover_station_obs_file was copied into the temporary directory: $temp_dir."
	fi
fi

#####______________________________option -b ______________________________#####
# if the -b option is set (i.e. the observation data for the base station are already present), then check if all required gps files exist
if [ $opt_bs == true ]
then
	if ! [ -f "$gps_data_dir_basestation/$base_station_obs_file" ]
	then
		echo "The observation data file for the basestation  $base_station_label for $year $month2 $day2 was not found at $gps_data_dir_basestation/$base_station_obs_file."
		echo  ""
    exit 1
	else
		cp "$gps_data_dir_basestation/$base_station_obs_file" "$temp_dir/$base_station_obs_file"
		#echo "The basestation's obs data $base_station_obs_file was copied into the temporary directory: $temp_dir."
	fi
fi

#####______________________________option -c ______________________________#####
# if the -c option is set (i.e. no conversion of raw gps data), then check if all required obs files exist
if [ $opt_cv == true ]
then
	if ! [ -f "$gps_data_dir_roverstation/$rover_station_obs_file" ] #if the option -r is set, this check hasn't to be done
	then
    echo "The observation data file for the roverstation  $rover_station_label for $year $month2 $day2 was not found at $gps_data_dir_roverstation/$rover_station_obs_file."
    echo ""
    exit 1
	else
		cp "$gps_data_dir_roverstation/$rover_station_obs_file" "$temp_dir/$rover_station_obs_file"
		#echo "The roverstation's obs data ( $rover_station_obs_file ) was copied into the temporary directory: $temp_dir."
	fi

	if ! [ -f "$gps_data_dir_basestation/$base_station_obs_file" ] #if the option -b is set, this check hasn't to be done
	then
		echo "The observation data file for the basestation  $base_station_label for $year $month2 $day2 was not found at $gps_data_dir_basestation/$base_station_obs_file."
    echo ""
    exit 1
	else
		cp "$gps_data_dir_basestation/$base_station_obs_file" "$temp_dir/$base_station_obs_file"
		#echo "The basestation's obs data ( $base_station_obs_file ) was copied into the temporary directory: $temp_dir."
	fi
fi


#################################################################################################################################################################
# Download the necessary data. First the GPS then the IGS
#################################################################################################################################################################

#####______________________________GPS download ______________________________#####
# download the raw gps data for the base and/or the rover station
if [ $opt_bs == false ] || [ $opt_rs == false ]
then

	echo "GPS RAW data download..."
fi

if [ $opt_bs == false ]
then
	echo "Download the GPS RAW data for base station $base_station_label for $year $month2 $day2"
	#y=$(python -B download_gps_data.py "-t" "$temp_dir" "-p" "$base_station_nr" "-l" "$base_station_label" "-d" "$day/$month/$year" "-n" "$base_station_deployment" "-s $base_gsn_server")
	y=$(python -B download_gps_data.py "-t" "$temp_dir" "-p" "$base_station_nr" "-l" "$base_station_label" "-d" "$day/$month/$year" "-n" "$base_station_deployment" "-s" "$base_gsn_server")
	#echo "$y"
	if ! [ -f "$temp_dir/$base_station_gps_file" ]
	then
    echo "Failed to download base station GPS RAW data."
		echo ""
    exit 1
	#else
	#	echo "Download successful."
	fi
fi

if [ $opt_rs == false ]
then
	echo "Download the GPS RAW data for rover station $rover_station_label for $year $month2 $day2"
	y=$(python -B download_gps_data.py "-t" "$temp_dir" "-p" "$rover_station_nr" "-l" "$rover_station_label" "-d" "$day/$month/$year" "-n" "$rover_station_deployment" "-s" "$rover_gsn_server")
	#echo "$y"
	if ! [ -f "$temp_dir/$rover_station_gps_file" ]
	then
    echo "Failed to download rover station GPS RAW data."
		echo ""
    exit 1
	#else
	#	echo "Download successful."
	fi
fi


#####______________________________Convert the raw gps data ______________________________#####

if [ $opt_cv == false ]
then
  if [ $opt_cv = false ] || [ $opt_rs == false ] || [ $opt_bs == false ]
  then
    echo "Conversion of RAW GPS data into RINEX format..."
  fi

  # check if the tool is found
  if ! [ -x "$rtklib_dir/convbin" ]
  then
    echo "The tool convbin wasn't found at $rtklib_dir/convbin."
  	echo ""
    exit 1
  fi
  
  base_station_convbin_exec_string="$rtklib_dir/convbin -d $temp_dir -hc $base_station_deployment -hm $base_station_label -hn $base_station_nr -ho ETH_Zurich -od -os -oi -ot -ol -o $base_station_obs_file $temp_dir/$base_station_gps_file"
  rover_station_convbin_exec_string="$rtklib_dir/convbin -d $temp_dir -hc $rover_station_deployment -hm $rover_station_label -hn $rover_station_nr -ho ETH_Zurich -od -os -oi -ot -ol -o $rover_station_obs_file $temp_dir/$rover_station_gps_file"

	if [ $opt_bs == false ] && [ $opt_rs == false ]
	then
    #Base Station
    #echo $base_station_convbin_exec_string
    echo "$base_station_convbin_exec_string"
    $base_station_convbin_exec_string > $temp_dir/log_convbin_base.txt 2>&1
    if ! [ -f "$temp_dir/$base_station_obs_file" ]
  	then
  		echo "The conversion of the base station's GPS file failed."
      echo ""
  		exit 1
  	else
  		echo "Converted the base station's GPS file."
  	fi
    #Rover Station
    #echo $rover_station_convbin_exec_string
    echo "$rover_station_convbin_exec_string"
    $rover_station_convbin_exec_string > $temp_dir/log_convbin_rover.txt 2>&1
    if ! [ -f "$temp_dir/$rover_station_obs_file" ]
  	then
  		echo "The conversion of the rover station's GPS file failed."
      echo ""
  		exit 1
  	else
  		echo "Converted the rover station's GPS file."
  	fi

  elif [ $opt_bs == false ] && [ $opt_rs == true ]
	then
    #Base Station
    #echo $base_station_convbin_exec_string
    echo "$base_station_convbin_exec_string"
    $base_station_convbin_exec_string > $temp_dir/log_convbin_bs.txt 2>&1
    if ! [ -f "$temp_dir/$base_station_obs_file" ]
  	then
  		echo "The conversion of the base station's GPS file failed."
      echo ""
  		exit 1
  	else
  		echo "Converted the base station's GPS file."
  	fi

  elif [ $opt_bs == true ] && [ $opt_rs == false ]
	then
    #Rover Station
    #echo $rover_station_convbin_exec_string
    echo "$rover_station_convbin_exec_string"
    $rover_station_convbin_exec_string > $temp_dir/log_convbin_rover.txt 2>&1
    if ! [ -f "$temp_dir/$rover_station_obs_file" ]
  	then
  		echo "The conversion of the rover station's GPS file failed."
  		echo ""
      exit 1
  	else
  		echo "Converted the rover station's GPS file."
  	fi
	fi
fi

#### output file sizes
ls -l $temp_dir'/'$rover_station_obs_file
ls -l $temp_dir'/'$base_station_obs_file


#####______________________________IGS download ___________________________#####
# download the IGS data for the date
# if the -d option is set (i.e. igs downloads shall be made), or check if all required igs files exist
if [ $opt_dl == true ]
then
  echo "IGS data download started..."
	# create the date-depending parts of the data paths as they are found in the download directories
  nav_folder=$(python -B -c 'import get_file_name; print get_file_name.get_navu_folderName( '$year', '$month' ,'$day')')

  sp3_folder=$(python -B -c 'import get_file_name; print get_file_name.get_sp3_folderName( '$year', '$month' ,'$day')')
  clk_folder=$(python -B -c 'import get_file_name; print get_file_name.get_clk_folderName( '$year', '$month' ,'$day')')
  erp_folder=$(python -B -c 'import get_file_name; print get_file_name.get_erp_folderName( '$year', '$month' ,'$day')')

  iono_folder=$(python -B -c 'import get_file_name; print get_file_name.get_iono_folderName( '$year', '$month' ,'$day')')
  dcb_folder=$(python -B -c 'import get_file_name; print get_file_name.get_dcb_folderName( '$year', '$month' )')

	# download the files
  d1=$(python -B download_igs_data.py "$dataserver_2" "$nav_folder" "$nav_file" "$temp_dir" )
	echo "$d1"
  if $opt_fi
  then
    d2=$(python -B download_igs_data.py "$dataserver_3" "$dcb_folder" "$dcb_file" "$temp_dir" )
	   echo "$d2"
  fi
	d3=$(python -B download_igs_data.py "$dataserver_1" "$sp3_folder" "$sp3_file" "$temp_dir" )
	echo "$d3"
  d4=$(python -B download_igs_data.py "$dataserver_1" "$clk_folder" "$clk_file" "$temp_dir" )
	echo "$d4"
  d5=$(python -B download_igs_data.py "$dataserver_4" "$iono_folder" "$iono_file" "$temp_dir" )
	echo "$d5"
  d6=$(python -B download_igs_data.py "$dataserver_1" "$erp_folder" "$erp_file" "$temp_dir" )
	echo "$d6"

	# if any of the downloads failed, stop this script's execution
	if [[ $d1 == Failed* ]] || [[ $d2 == Failed* ]] || [[ $d3 == Failed* ]] || [[ $d4 == Failed* ]] || [[ $d5 == Failed* ]] || [[ $d6 == Failed* ]]
  then
		echo "Failed to download all data."
		echo ""
    exit 1
  else
    echo "Download successful."
	fi

  uncompress "$temp_dir/$nav_file"
  if $opt_fi
  then
    uncompress "$temp_dir/$dcb_file"
  fi
  uncompress "$temp_dir/$sp3_file"
	uncompress "$temp_dir/$clk_file"
  uncompress "$temp_dir/$iono_file"
  uncompress "$temp_dir/$erp_file"

	# update the name of the (uncompressed) files
  nav_file=${nav_file%.Z}
  if $opt_fi
  then
    dcb_file=${dcb_file%.Z}
  fi
  sp3_file=${sp3_file%.Z}
	clk_file=${clk_file%.Z}
	iono_file=${iono_file%.Z}
  erp_file=${erp_file%.Z}

else
	#echo "The option without download (-d) is set."
  #bool indicating if all files are found
	files_not_found=false
  nav_file=${nav_file%.Z}
  if $opt_fi
  then
    dcb_file=${dcb_file%.Z}
  fi
  sp3_file=${sp3_file%.Z}
	clk_file=${clk_file%.Z}
  iono_file=${iono_file%.Z}
  erp_file=${erp_file%.Z}

#   if ! [ -f "$igs_data_dir/$nav_file" ]
#   then
#     echo $igs_data_dir
#     echo $igr_data_dir
#     echo "The nav data file for $year $month $day was not found at $igs_data_dir/$nav_file."
#     files_not_found=true
#   elif ! [ -f "$code_data_dir/$dcb_file" ]
#   then
#     if $opt_fi
#     then
#       echo "The dcb data file for $year $month $day was not found at $code_data_dir/$dcb_file."
#       files_not_found=true
#     fi
# 	elif ! [ -f "$igr_data_dir/$sp3_file" ]
# 	then
# 		echo "The sp3 data file for $year $month $day was not found at $igr_data_dir/$sp3_file."
# 		files_not_found=true
#   elif ! [ -f "$igr_data_dir/$clk_file" ]
# 	then
# 		echo "The clk data file for $year $month $day was not found at $igr_data_dir/$clk_file."
# 		files_not_found=true
#   elif ! [ -f "$igr_data_dir/$iono_file" ]
# 	then
#   	echo "The iono data file for $year $month $day was not found at $igr_data_dir/$iono_file."
# 	 	files_not_found=true
#   elif ! [ -f "$igr_data_dir/$erp_file" ]
# 	then
# 		echo "The erp data file for $year $month $day was not found at $igr_data_dir/$erp_file."
# 		files_not_found=true
# 	fi

# 	if [ $files_not_found == true ]
# 	then
# 		echo "Not all required files were found. Try with download option -d."
# 		#echo ""
#     exit 1
# 	else
#     cp "$igs_data_dir/$nav_file" "$temp_dir/$nav_file"
#     if $opt_fi
#     then
#       cp "$code_data_dir/$dcb_file" "$temp_dir/$dcb_file"
#     fi
#     cp "$igr_data_dir/$sp3_file" "$temp_dir/$sp3_file"
# 		cp "$igr_data_dir/$clk_file" "$temp_dir/$clk_file"
#     cp "$igr_data_dir/$iono_file" "$temp_dir/$iono_file"
#     cp "$igr_data_dir/$erp_file" "$temp_dir/$erp_file"
# 		echo "The IGS data (clk, sp3, nav, dcb, iono, erp) were copied from the archive."
# 	fi
fi

datetime_end=$(date '+%d/%m/%Y %H:%M:%S')
echo "Finished downloading data at: $datetime_end"
echo ""

exit 0
